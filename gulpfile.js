var elixir = require('laravel-elixir');
require('laravel-elixir-stylus');
require('laravel-elixir-vueify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.browserify('homeexpo.js', 'js/homeexpo.js')
    .stylus('../stylus/main.styl', 'css/main.css' ) 
    .stylus('../stylus/app.styl', 'css/app.css' )
    
    .styles([
        'node_modules/sweetalert/dist/sweetalert.css',
        'resources/assets/css/animate.min.css',
        'resources/assets/css/bootstrap.min.css',
        'resources/assets/css/font-awesome.css',
        'resources/assets/css/font-awesome.min.css',
        'resources/assets/css/owl.carousel.css',
        'resources/assets/css/owl.transitions.css',
        'resources/assets/css/prettyPhoto.css',
        'resources/assets/css/responsive.css',
        'css/app.css',
        'css/main.css',
    ], 'public/css/final.css', './')
    .version('public/css/final.css')
    
    .scripts([
        'node_modules/textarea-autosize/dist/jquery.textarea_autosize.min.js',
        'node_modules/sweetalert/dist/sweetalert.min.js',
        'js/homeexpo.js',
        ], 'js/main.js', './')
    .copy('public/build/css', 'build/css')
    .copy('public/build/rev-manifest.json', 'build/rev-manifest.json')
    .browserSync();
 
});

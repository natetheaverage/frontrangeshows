<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'i1521422_wp1');

/** MySQL database username */
define('DB_USER', 'i1521422_wp1');

/** MySQL database password */
define('DB_PASSWORD', 'D~S~RqD90z21]~3');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's4UJTwVXaVlA61RwU0dSDlg4AlZ52OZpUBe2DkE3TjCuS17r8ku3JPtSKSeTfJyc');
define('SECURE_AUTH_KEY',  'oXuZPAKMFeEvS50FApQZMmTAxFVg1e055LlkAY79uYi0nLVxDTC72MxZ3yKB4HKt');
define('LOGGED_IN_KEY',    '3p4UJEJaBXHEWGxa6hbnXb16yZRaGLznOzv1GPSJEs2PrDSFAJMnrBAiUJiLS02f');
define('NONCE_KEY',        'xYfm6e46IvBsU4Op5sAsDzmU6kMNbRXfeKX3JrgcftkYDiU4zgujmXCL9aFycV2B');
define('AUTH_SALT',        '89PcJgEiogyna4K8zeCoRFh06zs1XOtCdwb3oSyxrure4OTnh06tJ0YipS4qyyQM');
define('SECURE_AUTH_SALT', '4EB3zex8KlQcOREw3JRgZ7d2GhDxYuMP06mxo4yTDCgS7Cao4rhxJVX7OZWWZ1pZ');
define('LOGGED_IN_SALT',   'PNPbZ4vQh1vWHUjxDSHFNhga3r4932UBii27cfNBmeGdWM1ag6k8uQxst3Ihasiv');
define('NONCE_SALT',       'GUJvv9FBU65thw9aaihZkAFLc7FVny7vePjvKY1OE9cdXvd9Lro9VZvZpm4VS4YQ');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

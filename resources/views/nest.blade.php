<!DOCTYPE html>
<html>
    <head>
    <title>Home & Gadgets Expo|@yield('title')</title>
        @include('includes.head')
    </head>
    <body id="home" class="homepage">
        <div id="vue-container" >
            <header id="header">
                @include('partials.navbar') 
            </header><!--/header-->
            @section('content')
                
            @show
            <section id="get-in-touch" class="blue-hero">
            @include('partials.cta.get-in-touch')
            </section><!--/#get-in-touch-->

            <section id="contact">
                @include('partials.contact.contact')
            </section>
            
            @include('includes.footer')
        </div><!-- #vue-container -->
        @include('includes.imports')
    </body>
</html>
<nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button 
        type="button" 
        class="navbar-toggle" 
        data-toggle="collapse" 
        data-target=".navbar-collapse"
      >
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">
        <img src="/images/logo/WebLogo.png" alt="logo">
      </a>

        </div>
      <i v-if="saving" 
        style="margin: 20px 0 0 20px" 
        class="fa fa-refresh fa-2x fa-spin" 
        :class="[error ? text-danger : '']"
      ></i>


<!-- Split button -->
      <div class="btn-group tickets">
        <button 
          type="button" 
          class="btn btn-lg btn-primary dropdown-toggle" 
          data-toggle="dropdown" 
          aria-haspopup="true" 
          aria-expanded="false"
        >Event Tickets <span class="caret"></span>
        </button>
        <span class="sr-only">Toggle Drop-down</span>

        <ul class="dropdown-menu">
          <!-- <li><a href="http://homeandgadgetexpo.ticketleap.com/greeley/" target="_blank">Greeley, Co.</a></li>
          <li><a href="http://homeandgadgetexpo.ticketleap.com/coloradosprings/" target="_blank">Colorado Springs, Co. </a></li> -->
          <!-- <li><a href="http://homeandgadgetexpo.ticketleap.com/denverco/" target="_blank">Denver, Co.</a></li> -->
          <li><a href="https://homeandgadgetexpo.ticketleap.com/iowa/" target="_blank">Des Moines, Iowa</a></li>
          <!-- <li role="separator" class="divider"></li>
          <li><a href="#">Separated link</a></li> -->
        </ul>
      </div>


    <div class="collapse navbar-collapse navbar-right">
      <ul class="nav navbar-nav">
        
        <li class="scroll active"><a href="/">Home</a></li>
        <li class="scroll"><a href="/#events">Events</a></li>
        <!-- <li class="scroll">
          <a data-toggle="dropdown" 
            aria-haspopup="true" 
            aria-expanded="false"
          >Events
          <span class="caret"></span>
          </a>
          
          <span class="sr-only">Toggle Dropdown</span>
          <ul class="navbar dropdown-menu">
            
            <li class="dropdown-title">Get Tickets</li>
            <li>
              <a 
                href="http://homeandgadgetexpo.ticketleap.com/greeley/" 
                target="_blank"
              >Greeley, Co.</a>
            </li>
            <li>
              <a 
                href="http://homeandgadgetexpo.ticketleap.com/coloradosprings/" 
                target="_blank"
              >C. Springs, Co.</a>
            </li>
            <li>
              <a 
                href="http://homeandgadgetexpo.ticketleap.com/denverco/" 
                target="_blank"
              >Denver, Co.</a>
            </li>
          </ul>
        </li> -->
        
        <li class="scroll">
          <a data-toggle="dropdown" 
            aria-haspopup="true" 
            aria-expanded="false"
          >Prizes & Features
            <span class="caret"></span>
          </a>
          
          <span class="sr-only">Toggle Dropdown</span>
          <ul class="navbar dropdown-menu">

            <li class="scroll">
              <a 
                href="/files/HomeGadgetsContest.pdf" 
                target="_blank"
              > Enter to Win <i class="fa fa-dollar"></i>50K</a>
            </li>

            <li class="scroll">
              <a href="/features#vendor">Vendors</a>
            </li>

            <li class="scroll">
              <a href="/features#denver-highlight">Kristen Sollenne</a>
            </li>

          </ul>
        </li>
        <li class="scroll"><a href="/info#about">About</a></li>
        <li class="scroll"><a href="/vendors">Vendors</a></li>
        <li class="scroll"><a href="#get-in-touch">Contact</a></li>
      </ul>
    </div>
  </div><!--/.container-->
</nav><!--/nav-->
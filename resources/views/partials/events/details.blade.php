<section id="events-header" class="blue-hero">
  <div class="section-header">
    <h2 class="section-title text-center wow fadeInDown">
      <span
        v-if="!loggedIn" 
        v-html="truth.copyText.eventTitle"
      ></span>
      <textarea
        v-if="loggedIn" 
        v-model="truth.copyText.eventTitle"
        @click="[focusedField = 'eventTitle', focusedSection = '/api/copyText']"
        @keyup="save | debounce 300"
        rows="1"
      ></textarea>
    </h2>

    <p class="text-center wow fadeInDown">
      <span
        v-if="!loggedIn" 
        v-html="truth.copyText.eventTitle"
      ></span>
      <textarea
        v-if="loggedIn" 
        v-model="truth.copyText.eventSubTitle"
        @click="[focusedField = 'eventSubTitle', focusedSection = '/api/copyText']"
        @keyup="save | debounce 300"
        rows="2"
      ></textarea>
    </p>
  </div>
</section>

<div class="container">
  <div class="row" style=" margin:10px 0">
    <div 
      class="col-sm-6 col-md-4"
      style="padding-right: 0;" 
      v-for="(key, event) in $root.truth.events"
    >
        <div 
          class="team-member wow fadeInUp" 
          data-wow-duration="400ms" 
          data-wow-delay="0ms"
        >
        <center><a 
          class="btn btn-info"
          :href="'/events/'+event.url+'?id='+event.id"
        >Open</a></center>
        {{-- Title, Dates & Calendar Links --}}
        <datesbox 
          :event="event"
        ></datesbox>
        {{-- @include( 'partials.events.dates.greeley-spring-2016') --}}
        <br>
        {{-- Map and Address --}}
        <mapbox
          :event="event"
        ></mapbox>
        {{-- @include('partials.events.maps.greeley')--}}
      </div>
    </div>

  </div>
</div> 
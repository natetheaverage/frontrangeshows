<div class="col-md-3 booth-details">
  <ul>
    <h3>Booth Cost</h3>
    <li>Inline $900</li>
    <li>Corner $1000</li>
  </ul>
  <ul>
    <h3>Discounts</h3>
    <li>Multi-Booth is 5%</li>
  </ul>
  <ul>
    <h3>Other Details</h3>
    <li>Electric $75 (that is paid directly to National Western Complex)</li>
  </ul>
</div>

<div class="col-md-9 event-details">
          
  {{-- Title, Dates & Calendar Links --}}
  @include('partials.events.dates.denver-spring-2017')

  <br>

  {{-- Map and Address --}}
  @include('partials.events.maps.denver3')
  
</div>
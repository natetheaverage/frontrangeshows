<div class="col-md-3 booth-details">
  <ul>
    <h3>Booth Cost</h3>
    <li>Inline $600</li>
    <li>Corner $650</li>
  </ul>
  <ul>
    <h3>Discounts</h3>
    <li>Pre-Pay Discount is 5%</li>
    <li>Multi-Booth is 5%</li>
  </ul>
  <ul>
    <h3>Other Details</h3>
    <li>Electric $50</li>
  </ul>
</div>

<div class="col-md-9 event-details">
          
  {{-- Title, Dates & Calendar Links --}}
  @include('partials.events.dates.grandjunction-fall-2016')

  <br>

  {{-- Map and Address --}}
  @include('partials.events.maps.grandjunction')
  
</div>
<div class="col-md-3 booth-details">
  <ul>
    <h3>Booth Cost</h3>
    <li>Inline $1,100</li>
    <li>Corner $1,2500</li>
  </ul>
  <ul>
    <h3>Discounts</h3>
    <li>Early-Bird Discount 5% 
    <br><small>(Deadline - August 1st)</small></li>
    <li>Multi-Show Discount 5% 
    <br><small>(No Multi-Booth Available) </small></li>
    <li>Pre-pay Discount 5% 
    <br><small>(Must pay in full upon application)</small></li>
  </ul>
  <ul>
    <h3>Other Details</h3>
    <li>Electric $75</li>
  </ul>
</div>

<div class="col-md-9 event-details">
          
  {{-- Title, Dates & Calendar Links --}}
  @include('partials.events.dates.sandy-spring-2017')

  <br>

  {{-- Map and Address --}}
  @include('partials.events.maps.sandy')
  
</div>
<div class="col-md-3 booth-details">
  <ul>
      <h3>Booth Cost</h3>
      <li>Inline $750</li>
      <li>Corner $850</li>
    </ul>
    <ul>
      <h3>Discounts</h3>
      <li>Multi-Booth is 5%</li>
    </ul>
    <ul>
      <h3>Other Details</h3>
      <li>Electric $75</li>
    </ul>
</div>


<div class="col-md-9 event-details">
          
  {{-- Title, Dates & Calendar Links --}}
  @include('partials.events.dates.greeley-spring-2016')

  <br>

  {{-- Map and Address --}}
  @include('partials.events.maps.greeley')
  
</div>
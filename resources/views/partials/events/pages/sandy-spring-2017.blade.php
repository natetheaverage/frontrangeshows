@extends('nest')
@section('title', 'Sandy Spring 2017')

@section('content')
    @parent
    <header id="header">
        @include('partials.navbar')  
    </header><!--/#header-->

  <div 
    style="margin-top:50px"
    class="wow fadeInUp" 
    data-wow-duration="400ms" 
    data-wow-delay="0ms"
  >
    <div class="team-info">
    <div 
      class="row"
      style="display:flex; justify-content:space-between;"
    >
      <a href="/events/layton-spring-2017">
      <i class="fa fa-icon fa-arrow-left"></i>Layton<br> Spring 2017</a>
      <a href="/events/desmoines-spring-2017">Des Moines<br> Spring 2017 
      <i class="fa fa-icon fa-arrow-right"></i>
      </a>
    </div>
    
      {{-- Title, Dates & Calendar Links --}}
      @include('partials.events.dates.sandy-spring-2017')
    <br>

        <div class="container" style="border-bottom: 1px solid #dbdbdb">
          <div class="section-header">
            <h2 class="section-title text-center wow fadeInDown">Event Details</h2>
            <p 
              class="text-center wow fadeInDown" 
              v-if="eventsReady"
              v-html="truth.events[6].details"
            ></p>
          </div>
          <div class="row">
            <div class="features">
              <div 
                class="col-md-4 col-sm-12 wow fadeInUp" 
                data-wow-duration="300ms" 
                data-wow-delay="0ms"
              >
                <div class="media service-box">
                  <div class="pull-left">
                    <i class="fa fa-dollar"></i>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">Ticket Price!</h4>
                    <ul>
                      <li><small>Adults</small></li>
                    </ul>
                  </div>
                </div>
              </div><!--/.col-md-4-->

              <div 
                class="col-md-4 col-sm-12 wow fadeInUp" 
                data-wow-duration="300ms" 
                data-wow-delay="0ms"
              >
                <div class="media service-box">
                  <div class="pull-left">
                    <i class="fa fa-ticket"></i>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">Get Your Ticket Now!</h4>
                    
                    <a 
                      class="btn btn-success"
                      target="_blank"
                      v-if="eventsReady"
                      :href="truth.events[6].ticket_vendor"
                      v-text="$root.truth.copyText.ticketButton"
                    ></a>
                  </div>
                </div>
              </div><!--/.col-md-4-->

              <div 
                class="col-md-4 col-sm-12 wow fadeInUp" 
                data-wow-duration="300ms" 
                data-wow-delay="100ms">
                <div class="media service-box">
                  <div class="pull-left">
                    <i class="fa fa-car"></i>
                  </div>
                  <div class="media-body">
                    <h4 class="media-heading">Parking will be a breeze!</h4>
                    <!-- <ul>
                      <li><small>Parking $10 </small></li>
                    </ul> -->
                  </div>
                </div>
              </div>
            </div>
          </div><!--/.row--> 
        </div>
        
        <br>
          
        {{-- Map and Address --}}
        @include('partials.events.maps.sandy')
  
      </div>
      
@endsection

<div class="col-sm-12">
  <h1 class="column-title">Event Details</h1>
  <div role="tabpanel">

    <ul class="nav main-tab nav-justified" role="tablist">
     <!--  <li role="presentation" class="active">
        <a 
        v-on:click="$root.initMap(1) | debounce 500"
        href="#details-tab1" role="tab" data-toggle="tab" aria-controls="details-tab1" aria-expanded="true">Greeley <br />(Spring 2016)</a>
      </li> -->
      <li role="presentation" v-for="(key, event) in truth.events">
        <a
          v-on:click="$root.initMap(key) | debounce 500"
          :href="'#details-tab'+key" role="tab" 
          data-toggle="tab" 
          :aria-controls="'details-tab'+key" 
          aria-expanded="false"
          v-html="event.name"
        ></a>
      </li>
      <!-- <li role="presentation">
        <a
          v-on:click="$root.initMap(3) | debounce 500"
          href="#details-tab3" role="tab" data-toggle="tab" aria-controls="details-tab3" aria-expanded="false">Denver<br />(Spring 2016)</a>
      </li>
      <li role="presentation">
        <a
          v-on:click="$root.initMap(4) | debounce 500"
          href="#details-tab4" role="tab" data-toggle="tab" aria-controls="details-tab4" aria-expanded="false">Des Moines<br />(Spring 2016)</a>
      </li>
      <li role="presentation">
        <a
          v-on:click="$root.initMap(5) | debounce 500"
          href="#details-tab5" role="tab" data-toggle="tab" aria-controls="details-tab5" aria-expanded="false">Layton<br />(Fall 2016)</a>
      </li>
      <li role="presentation">
        <a
          v-on:click="$root.initMap(6) | debounce 500"
          href="#details-tab6" role="tab" data-toggle="tab" aria-controls="details-tab6" aria-expanded="false">Grand Junction Holiday Expo<br />(Fall 2016)</a>
      </li>
      <li role="presentation">
        <a
          v-on:click="$root.initMap(7) | debounce 500"
          href="#details-tab7" role="tab" data-toggle="tab" aria-controls="details-tab7" aria-expanded="false">Cheyenne Holiday Expo<br />(Fall 2016)</a>
      </li>
      <li role="presentation">
        <a
          v-on:click="$root.initMap(8) | debounce 500"
          href="#details-tab8" role="tab" data-toggle="tab" aria-controls="details-tab8" aria-expanded="false">Denver Holiday Expo<br />(Winter 2016)</a>
      </li>
      <li role="presentation">
        <a
          v-on:click="$root.initMap(9) | debounce 500"
          href="#details-tab9" role="tab" data-toggle="tab" aria-controls="details-tab9" aria-expanded="false">Layton<br />(Spring 2017)</a>
      </li>
      <li role="presentation">
        <a
          v-on:click="$root.initMap(10) | debounce 500"
          href="#details-tab10" role="tab" data-toggle="tab" aria-controls="details-tab10" aria-expanded="false">Sandy<br />(Spring 2017)</a>
      </li>
      <li role="presentation">
        <a
          v-on:click="$root.initMap(11) | debounce 500"
          href="#details-tab11" role="tab" data-toggle="tab" aria-controls="details-tab11" aria-expanded="false">Des Moines<br />(Spring 2017)</a>
      </li>
      <li role="presentation">
        <a
          v-on:click="$root.initMap(12) | debounce 500"
          href="#details-tab12" role="tab" data-toggle="tab" aria-controls="details-tab12" aria-expanded="false">Denver<br />(Spring 2017)</a>
      </li> -->
    </ul>

    {{-- end tab content --}}
    <div 
      id="tab-content" 
      class="tab-content row"
    >
      
      {{-- start tab 1 
      <div 
        v-for="(key, event) in truth.events"
        :id="'details-tab'+key" 
        role="tabpanel" 
        class="tab-pane fade active in" 
        aria-labelledby="details-tab'+key" 
      >
         partials.events.vendors.greeley-spring-2016
        
       
    </div> {{-- end tab 1 --}}

      {{-- start tab 2 
      <div 
        id="details-tab0" 
        role="tabpane0" 
        class="tab-pane fade" 
        aria-labelledby="details-tab0" >

        @include('partials.events.vendors.denver-spring-2016')

      </div> {{-- end tab 0 --}}

      {{-- start tab 0 --}}
      <div 
        id="details-tab0" 
        role="tabpane0" 
        class="tab-pane fade" 
        aria-labelledby="details-tab0" >

        @include('partials.events.vendors.desmoines-spring-2016')
        
      </div> {{-- end tab 0 --}}


      {{-- start tab 1 --}}
      <div 
        id="details-tab1" 
        role="tabpane1" 
        class="tab-pane fade" 
        aria-labelledby="details-tab1" >
        
        @include('partials.events.vendors.layton-fall-2016')

      </div>{{-- end tab 1 --}}


      {{-- start tab 2 --}}
      <div 
        id="details-tab2" 
        role="tabpane2" 
        class="tab-pane fade" 
        aria-labelledby="details-tab2" >
        
        @include('partials.events.vendors.grandjunction-fall-2016')

      </div>{{-- end tab 2 --}}


      {{-- start tab 3 --}}
      <div 
        id="details-tab3" 
        role="tabpane3" 
        class="tab-pane fade" 
        aria-labelledby="details-tab3" >
        
        @include('partials.events.vendors.cheyenne-fall-2016')
        
      </div>{{-- end tab 3 --}}

      {{-- start tab 4 --}}
      <div 
        id="details-tab4" 
        role="tabpane4" 
        class="tab-pane fade" 
        aria-labelledby="details-tab4" >

        @include('partials.events.vendors.tucson-fall-2016')
        
      </div> {{-- end tab 4 --}}

      {{-- start tab 5 --}}
      <div 
        id="details-tab5" 
        role="tabpane5" 
        class="tab-pane fade" 
        aria-labelledby="details-tab5" >
        
        @include('partials.events.vendors.denver-winter-2016')
        ß
      </div>{{-- end tab 5 --}}


      {{-- start tab 6 --}}
      <div 
        id="details-tab6" 
        role="tabpane6" 
        class="tab-pane fade" 
        aria-labelledby="details-tab6" >

        @include('partials.events.vendors.layton-spring-2017')
        
      </div> {{-- end tab 6 --}}


      {{-- start tab 7 --}}
      <div 
        id="details-tab7" 
        role="tabpane7" 
        class="tab-pane fade" 
        aria-labelledby="details-tab7" >

        @include('partials.events.vendors.sandy-spring-2017')
        
      </div> {{-- end tab 7 --}}


      {{-- start tab 8 --}}
      <div 
        id="details-tab8" 
        role="tabpane8" 
        class="tab-pane fade" 
        aria-labelledby="details-tab8" >
        
        @include('partials.events.vendors.desmoines-spring-2017')

      </div>{{-- end tab 8 --}}

      {{-- start tab 9 --}}
      <div 
        id="details-tab9" 
        role="tabpane9" 
        class="tab-pane fade" 
        aria-labelledby="details-tab9" >

        @include('partials.events.vendors.denver-spring-2017')
        
      </div> {{-- end tab 9 --}}


    </div>{{-- end tab content --}}
  </div>{{-- end tab panel --}}
</div>{{-- end col-12 container --}}
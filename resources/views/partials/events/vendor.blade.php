<div class="container">
  <div class="section-header">
      <h2 class="section-title text-center wow fadeInDown">
      <center>
          <span
              v-if="!loggedIn" 
              v-html="truth.copyText.vendorsTitle"
          ></span>
          <textarea
              v-if="loggedIn" 
              v-model="truth.copyText.vendorsTitle"
              @click="focusedField = 'vendorsTitle'"
              @keyup="save | debounce 300"
              rows="1"
          ></textarea>
      </h2>
      <p class="wow text-center fadeInDown">
          <span
              v-if="!loggedIn" 
              v-html="truth.copyText.vendorsSubTitle"
          ></span>
          <textarea
              v-if="loggedIn" 
              v-model="truth.copyText.vendorsSubTitle"
              @click="focusedField = 'vendorsSubTitle'"
              @keyup="save | debounce 300"
              rows="1"
          ></textarea>
      </p>
  </div> 
  
  <div class="text-center">
      <ul class="vendor-filter">
          <li><a class="active" href="#" data-filter="*">All Vendors</a></li>
          <li><a href="#" data-filter=".outdoor">Outdoor</a></li>
          <li><a href="#" data-filter=".indoor">Indoor</a></li>
          <li><a href="#" data-filter=".gadgets">Gadgets</a></li>
      </ul><!--/#vendor-filter-->
  </div>

  <div class="vendor-items">
  <featuredvendor 
  v-for="vendor in truth.features.vendors.list"
  :vendor="vendor"
  ></featuredvendor>
      
  </div>
</div><!--/.container-->
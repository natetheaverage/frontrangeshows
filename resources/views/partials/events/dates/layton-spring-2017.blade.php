<center>
  <h1>Layton, Utah</h1>
  <h2>Mar. 31st-Apr. 1st 2017</h2>
</center>
  
<ul class="social-icons">
  <div class="dates">
    <li><center>
      <div>
        <a href="#">
          <span class="fa-stack fa-2x">
            <i class="fa fa-calendar-o fa-stack-1x"></i>
            <span class="fa-stack">3/31</span>
          </span>
        </a>
      </div>
      <p>10-6pm</p></center>
    </li>
    <li><center>
        <a href="#">
          <span class="fa-stack fa-2x">
            <i class="fa fa-calendar-o fa-stack-1x"></i>
            <span class="fa-stack">4/1</span>
          </span>
        </a>
      <p >10-6pm</p></center>
    </li>
    
  </div>
</ul>
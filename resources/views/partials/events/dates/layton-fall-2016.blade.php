<center>
  <h1>Layton, Utah</h1>
  <h2>Aug. 26th-27th 2016</h2>
</center>
  
<ul class="social-icons">
  <div class="dates">
    <li><center>
      <div>
        <a href="#">
          <span class="fa-stack fa-2x">
            <i class="fa fa-calendar-o fa-stack-1x"></i>
            <span class="fa-stack">8/26</span>
          </span>
        </a>
      </div>
      <p>10-6pm</p></center>
    </li>
    <li><center>
        <a href="#">
          <span class="fa-stack fa-2x">
            <i class="fa fa-calendar-o fa-stack-1x"></i>
            <span class="fa-stack">8/27</span>
          </span>
        </a>
      <p >10-6pm</p></center>
    </li>
    
  </div>
</ul>
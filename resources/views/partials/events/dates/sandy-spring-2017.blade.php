<center>
  <h1>Sandy, Utah</h1>
  <h2>Apr. 21st-23rd 2017</h2>
</center>
  
<ul class="social-icons">
  <div class="dates">
    <li><center>
      <div>
        <a href="#">
          <span class="fa-stack fa-2x">
            <i class="fa fa-calendar-o fa-stack-1x"></i>
            <span class="fa-stack">4/21</span>
          </span>
        </a>
      </div>
      <p>10-6pm</p></center>
    </li>
    <li><center>
      <div>
        <a href="#">
          <span class="fa-stack fa-2x">
            <i class="fa fa-calendar-o fa-stack-1x"></i>
            <span class="fa-stack">4/22</span>
          </span>
        </a>
      </div>
      <p>10-6pm</p></center>
    </li>
    <li><center>
        <a href="#">
          <span class="fa-stack fa-2x">
            <i class="fa fa-calendar-o fa-stack-1x"></i>
            <span class="fa-stack">4/23</span>
          </span>
        </a>
      <p >10-6pm</p></center>
    </li>
    
  </div>
</ul>
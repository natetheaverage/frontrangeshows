<div class="container">
    <div class="text-center">
        <h2 class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">
        <span>HEY</span> ROBOT! 
        <br>GO MOW MY LAWN, 
        <br>NOW VACUUM THE HOUSE!</h2>
        <p class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="100ms">Sundays are for family, friends, football and BBQ’s, Check out products to give you more freedom.</p>
        <p class="wow fadeInUp" data-wow-duration="300ms" data-wow-delay="200ms">
            <a class="btn btn-primary btn-lg" href="#vendor">See More Vendors</a>
        </p>
        <a href="#vendor">
            <img 
                data-pin-nopin="true" 
                src="images/cta2/cta2-mow-roomba.png" 
                style="margin-top: -100px;"
                class="img-responsive wow fadeIn" 
                data-wow-duration="300ms" 
                data-wow-delay="300ms">
       </a>
    </div>
</div>
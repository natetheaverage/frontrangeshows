<div class="container">
    <div class="row">
        <div class="col-sm-9">
            <h1>
                <span
                  v-if="!loggedIn" 
                  v-html="truth.copyText.introTitle"
                ></span>
                <textarea
                  v-if="loggedIn" 
                    v-model="truth.copyText.introTitle"
                    @click="[focusedField = 'introTitle', focusedSection = '/api/copyText']"
                    @keyup="save | debounce 300"
                    rows="1"
                ></textarea>
            </h1>
            <h2>
                <span
                  v-if="!loggedIn" 
                  v-html="truth.copyText.introSubTitle"
                ></span>
                <textarea
                  v-if="loggedIn" 
                    v-model="truth.copyText.introSubTitle"
                    @click="[focusedField = 'introSubTitle', focusedSection = '/api/copyText']"
                    @keyup="save | debounce 300"
                    rows="1"
                ></textarea>
            </h2>
            <p>
                <span
                  v-if="!loggedIn" 
                  v-html="truth.copyText.introText"
                ></span>
                <textarea
                  v-if="loggedIn" 
                    v-model="truth.copyText.introText"
                    @click="[focusedField = 'introText', focusedSection = '/api/copyText']"
                    @keyup="save | debounce 300"
                ></textarea>
            </p>
        </div>
        <div class="col-sm-3 text-right">
            <a 
                class="btn btn-primary btn-lg" 
                href="/info"
                v-text="truth.copyText.introButtonText"
            >
                
            </a>
            {{-- <a class="btn btn-primary btn-lg" href="/vendors">Vendor Application!</a> --}}
        </div>
    </div>
</div>

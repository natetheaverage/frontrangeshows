<div class="container-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-offset-2">
                @include('partials.contact.quick-form')
            </div>
            <div class="col-sm-4">
                <div class="contact-form">
                    <h3>Contact Info</h3>
                    <address>
                      <strong>Nationwide Expos</strong>
                      <br>891 14th St Suite 4204
                      <br>Denver, CO 80202
                      <br>
                      Email: <a target="_blank" href="mailto:info@nationwideexpos.com">
                      info@nationwideexpos.com</a><br>
                      <abbr title="Phone">Colorado:</abbr> (720)316-2757 
                      <br>
                      <abbr title="Phone">Iowa:</abbr> (515) 505-7035 
                      <br>
                      <abbr title="Phone">Utah:</abbr> (801)660-4448 
                      <br>
                      <abbr title="Phone">Wyoming:</abbr> (307)459-1097 
                      <br>
                      <abbr title="Fax"> Fax:</abbr> (303)942-3599
                    </address>
                </div>
                @include('partials.contact.social')
            </div>
        </div>
    </div>
</div>


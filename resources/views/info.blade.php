@extends('nest')
@section('title', 'Home')

@section('content')
    @parent

    {{-- <section id="services" >
        @include('partials.info.services')
    </section><!--/#services--> --}}

    <section id="events">
        @include('partials.events.details')
    </section><!--/#events-->

    <section id="about">
        @include('partials.info.about')
    </section><!--/#about-->

    {{-- <!-- <section id="blog">
        <div class="container">
            @include('partials.info.news')
        </div>
    </section> --> --}}


@endsection

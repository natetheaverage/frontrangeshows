<footer id="footer">
  <div class="container">
      <div class="row">
          <div class="col-sm-6">
              <p>&copy; 2015 Home & Gadgets Expo.</p>
              <p>Designed by <a target="_blank" href="http://www.natetheaverage.com/" title="Dream Alchemist, Keep Bouncing & Stay Delicious">NateTheAverage</a></p>
              <button class="btn btn-info" @click="adminLogin">
                  <i class="fa" :class="loggedIn ? 'fa-unlock' : 'fa-lock'"></i>
              </button>
              
          </div>
      </div>
      @include('partials.contact.social')
  </div>

  

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');  
    ga('create', 'UA-74208204-1', 'auto');
    ga('send', 'pageview');
  </script>

  <!-- Google Tag Manager -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KP5HZR"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-KP5HZR');</script>
  <!-- End Google Tag Manager -->
 
</footer><!--/#footer-->
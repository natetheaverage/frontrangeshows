
@extends('nest')
@section('title', 'Home')

@section('content')
    @parent

    

    <section id="main-slider">
        @include('partials.cta.slider')
    </section><!--/#main-slider-->

    <section id="cta" class="wow fadeIn">
        @include('partials.intro')
    </section><!--/#cta-->

    <section id="events">
            @include('partials.events.details')
    </section><!--/#events-->

    <section id="cta3">
        @include('partials.cta.cta3')
    </section> 

    {{-- <section id="services" >
            @include('partials.info.services')
    </section><!--/#services--> --}}

    {{-- <!-- <section id="animated-number">
         @include('partials.info.facts') 
    </section> --> --}}

    {{-- <!-- <section id="blog">
        <div class="container">
            @include('partials.info.news')
        </div>
    </section> --> --}}

    {{-- <!--<section id="testimonial">
        @include('includes.testimonials')
    </section>/#testimonial--> --}}

@endsection

@extends('nest')
@section('title', 'Vendors')

@section('content')
    @parent

    <registration v-if="visible"></registration>


    <section id="work-process">
        @include('partials.info.work-process')
    </section><!--/#work-process-->

    <section id="events">
        @include('partials.events.vender-details')
        <div class="divider"></div>
        @include('partials.info.faq')
    </section><!--/#meet-team-->

    {{-- <section id="pricing">
            @include('partials.events.pricing')
    </section> --}}<!--/#pricing-->

   {{-- <!-- <section id="animated-number">
         @include('partials.news.facts') 
    </section> --> --}}

    {{-- <!--<section id="testimonial">
        @include('partials.news.testimonials')
    </section>/#testimonial--> --}}
      
      
@endsection
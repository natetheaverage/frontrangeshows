@extends('nest')
@section('title', 'Home')

@section('content')
    @parent

    <section id="cta2"> 
        @include('partials.cta.cta2')
    </section>
 
    <section id="features">
         @include('partials.features.home')
    </section><!--/#features-->

    <section id="vendor">
        @include('partials.events.vendor')
    </section><!--/#vendor-->

    <section id="denver-highlight">
        @include('partials.highlights.kristen-apperance')
    </section><!--/#meet-team-->
    
    <section id="cta1"> 
        @include('partials.cta.cta1')
    </section>

    <section style="padding-top:30px" id="features2">
         @include('partials.features.gadgets')
    </section>

    <!-- Pintrest Code -->
    <script async defer data-pin-hover="true" src="//assets.pinterest.com/js/pinit.js"></script>

@endsection

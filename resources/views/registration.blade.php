@extends('nest')
@section('title', 'Home')

@section('content')
    @parent


    <registration v-if="registrationVisible"></registration>
    
@endsection
<!DOCTYPE html>
<html> 
    <head>
    <title>Home & Gadgets Expo|@yield('title')</title>
        @include('includes.head')
    </head>
    <body id="home" class="homepage">
        <div id="vue-container">

            <header id="header">
                @include('partials.navbar') 
            </header><!--/header-->

            @section('content')
                
            @show

            @include('includes.footer')
        </div><!-- #vue-container -->
        @include('includes.imports')
    </body>
</html>
module.exports = {

  bladeTemplates: '',

  copyText: {
    introTitle: 'Welcome to Nationwide',
    introSubTitle: 'Home & Gadgets Expo',
    introText: 'We host conventions focused on household products and useful new technologies. Providing you with the opportunity to be light years ahead of the Joneses.',
    introButtonText: 'Learn More About US!',
    vendorsTitle: 'FEATURED VENDORS',
    vendorsSubTitle: 'Here are some of our vendor highlights.',
    eventTitle: 'Upcoming Events',
    eventSubTitle: 'Here are the main details for the upcoming spring 2016-2017 show seasons. Have any questions please ask.',
    eventDetailsTitle: 'Event Details',
    eventTicketsTitle: 'Event Tickets',
    ticketButton: 'Ticketing Site'
  },// End Home

  events:{
  	// {
   //    name: 'Des Moines Spring 2016',
  	// 	city: 'Des Moines',
  	// 	state: 'Idaho',
  	// 	details: "Sky's not the limit, it's only the beginning at the Des Moines, IA Home and Gadget Expo! <br />Our convention focuses on bringing you unique house hold products as well as cutting edge technologies... So you can forget about keeping up with the Jonses, this expo will have you light years ahead of the Jonses!  Don’t forget to check out our kids section featuring free face paintings, engaging activities and games! Stage presentations by local and national experts will be featured all 3 days and will both entertain and amaze you… <br />We hope to see you there!",	  		
  	// 	season: 'Spring ',
  	// 	month: 'June',
   //    year: '2016',
   //    venue: 'Iowa Events Center ',
   //    street: '730 3rd St.',
   //    zip: '50309',
   //    featureOne: '0',
   //    featureTwo: '0',
   //    selected: false,
   //    dates:[],
  	// },{
   //    name: 'Greeley Spring 2016',
  	// 	city: 'Greeley',
  	// 	state: 'Colorado',
  	// 	details: "The Home and Gadget Expo in Greeley, CO features all of your favorite’s attractions a home show customarily brings you with a technical twist! <br />From home renovation ideas and amazing innovative tools to help maximize the use of your space, all the way to the latest and greatest in cutting- edge technology, our show has something for everyone… Bring the kiddos along and see what fun they can get into at our KIDS section featuring engaging activities, FREE face painting and games! Stage presentations by local Greely and national experts will be featured all 3 days and will both entertain and amaze you… <br />We hope to see you there!",	  		
  	// 	season: 'Spring',
  	// 	month: 'June',
   //    year: '2016',
   //    venue: 'Iowa Events Center ',
   //    street: '730 3rd St.',
   //    zip: '50309',
   //    featureOne: '0',
   //    selected: false,
   //    eventBright: 'http://www.eventbrite.com/e/colorado-springs-home-gadget-expo-tickets-21242014437?aff=SiteCOS',
   //    tickets:[ 'Adults(13+) $5', 'Children Free', '(Friday Show) Senors FREE!', '(Friday Show) Military FREE!'],
   //    parking: 'Plenty of free parking will be available!',
   //    vendor: [
   //      {
   //        label: 'Regular Vendors',
   //        details:['Inline $750', 'Corner $850']
   //      },{
   //        label: 'Craft Vendors',
   //        details:['Inline $500', 'Corner $600']
   //      },{
   //        label: 'Discounts',
   //        details:[
   //        'Early Bird Discount is 5% through June 1, 2016', 
   //        'Pre-Pay Discount is 5% though June 1, 2016',
   //        'Multi-Booth Discount is 5%'
   //        ]
   //      },{
   //        label: 'Other Details',
   //        details:[
   //        'Electric $75 (that is paid directly to National Western Complex)', 
   //        ]
   //      },
   //    ],
   //    dates:[],
  	// },{
   //    name: 'Colorado Springs Spring 2016',
  	// 	city: 'Colorado Springs',
  	// 	state: 'Colorado',
  	// 	details: "The Home and Gadget Expo in Colorado Springs, CO features all of your favorite’s attractions a home show customarily brings you with a technical twist! <br />Our amazing vendors will be bringing you everything from home renovation ideas and tools you never knew existed to make your life easier, all the way to the latest and greatest in cutting- edge technology; our show has something for everyone… We are super excited to feature a KIDS section featuring engaging activities and games! Stage presentations by local and national experts will be featured all 3 days and will both entertain and amaze you… <br />We can’t wait to see you there!",	  		
  	// 	season: 'Spring',
  	// 	date: '1234-12-11',
   //    venue: 'Iowa Events Center ',
   //    street: '730 3rd St.',
   //    zip: '50309',
   //    featureOne: '0',
   //    featureTwo: '0',
   //    selected: false,
   //    dates:[],
  	// },{
   //    name: 'Denver Spring 2016',
  	// 	city: 'Denver',
  	// 	state: 'Colorado',
  	// 	details: 'The Home and Gadget Expo in Denver, CO features all of your favorite’s attractions a home show customarily brings you with a technical twist! <br />Our top of the line vendors will be bringing you everything from home renovation ideas and amazing innovative tools to help maximize the use of your space, all the way to the latest and greatest in cutting- edge technology; our show has something for everyone… We even have a KIDS section featuring engaging activities and games! Stage presentations by local and national experts will be featured all 3 days and will both entertain and amaze you… <br />We are excited to see you there!',
  	// 	season: 'Spring',
  	// 	month: 'June',
   //    year: '2016',
   //    venue: 'Iowa Events Center ',
   //    street: '730 3rd St.',
   //    zip: '50309',
   //    featureOne: '0',
   //    featureTwo: '0',
   //    dates:[],
  	// },{
   //    name: 'Holiday Show Winter 2016',
  	// 	city: 'Denver',
  	// 	state: 'Colorado',
  	// 	details: 'The Home and Gadget Expo in Denver, CO features all of your favorite’s attractions a home show customarily brings you with a technical twist! <br />Our top of the line vendors will be bringing you everything from home renovation ideas and amazing innovative tools to help maximize the use of your space, all the way to the latest and greatest in cutting- edge technology; our show has something for everyone… We even have a KIDS section featuring engaging activities and games! Stage presentations by local and national experts will be featured all 3 days and will both entertain and amaze you… <br />We are excited to see you there!',
  	// 	season: 'Winter',
  	// 	month: 'June',
   //    year: '2016',
   //    venue: 'Iowa Events Center ',
   //    street: '730 3rd St.',
   //    zip: '50309',
   //    featureOne: '0',
   //    featureTwo: '0',
   //    selected: false,
   //    dates:[],
  	// },

  },

  vendorAssets: {
    applications: [
      { 
        label: 'Greeley (Spring 2016)',
        link: '/files/Greeley-Spring2016-HomeGadgetsExpo-Application.pdf'
      },
      {
        label: 'Colorado Springs (Spring 2016)',
        link: '/files/ColoradoSprings-Spring2016-HomeGadgetsExpo-Application.pdf'
      },
      {
        label: 'Denver (Spring 2016)',
        link: '/files/Denver-Spring2016-HomeGadgetsExpo-Application.pdf'
      },
      {
        label: 'Des Moines (Spring 2016)',
        link: '/files/DesMoines-Spring2016-HomeGadgetsExpo-Application.pdf'
      },
      {
        label: 'Denver Holiday Expo (Winter 2016)',
        link: '/files/Denver-2016-Holiday-HomeGadgetsExpo-Application.pdf'
      },
    ],
    floorplans: [
      {
        label: 'Greeley',
        thumb: 'greeley.png',
        link: 'GreeleyExpoFloorplan2016.pdf',
        active: true
      },{
        label: 'Co. Springs',
        thumb: 'colorado-springs.png',
        link: 'ColoradoSpringsExpoFloorplan2016.pdf',
        active: false
      },{
        label: 'Denver',
        thumb: 'denver.png',
        link: 'DenverExpoFloorplan2016.pdf',
        active: false
      },{
        label: 'Des Moines',
        thumb: 'desmoines2016.png',
        link: 'DesMoinesExpoFloorplan2016.pdf',
        active: false
      },
    ],
    faqs: [
      {
        label: 'When does exhibitor registration begin?',
        content: 'Registration for the Spring 2016 shows has begun! All booth space is first come first serve so register as soon as possible.',
        active: true
      },{
        label: "What's included in booth space?",
        content: "Booths include: <ul><li>Standard pipe and draping</li> <li>A basic exhibitor sign</li></ul> *Additional needs such as electricity are rented separately.",
        active: false
      },{
        label: "What's required to reserve booth space?",
        content: '50% of booth fees are required upfront to reserve booth space with the remaining 50% due 30 days prior to each show.<br /> Enjoy a 5% discount off booth fees by paying total upfront.',
        active: false
      },{
        label: "Are any booth discounts available?",
        content: 'YES! Multi-booth as well as multi-show discounts are available. Please contact us at 720-316-2757 for details.',
        active: false
      },

    ]
  },

  features: {
  	vendors: {
  		title: "FEATURED VENDORS",
  		subTitle: "To many to list them all but here are a few to look forword to.",
  		list: null,
  		// [{
	  	// 		title: "Hoverboards",
	  	// 		img: "images/vendor/01.jpg",
	  	// 		description: "",
	  	// 		tags: "gadgets"
	  	// 	},{
	  	// 		title: "LeafGaurd",
	  	// 		img: "images/vendor/02.jpg",
	  	// 		description: "",
	  	// 		tags: "gadgets outdoor"
	  	// 	},{
  		// 	title: "VR Glasses",
  		// 	img: "images/vendor/03.jpg",
  		// 	description: "",
  		// 	tags: "gadgets"
  		// },{
  		// 	title: "Energy Saving Windows",
  		// 	img: "images/vendor/04.jpg",
  		// 	description: "",
  		// 	tags: "outdoor indoor"
  		// },{
  		// 	title: "Heat Depot",
  		// 	img: "images/vendor/05.jpg",
  		// 	description: "",
  		// 	tags: "indoor"
  		// },{
  		// 	title: "Pillows",
  		// 	img: "images/vendor/06.jpg",
  		// 	description: "",
  		// 	tags: "indoor"
  		// },{
  		// 	title: "Shoe Tubz",
  		// 	img: "images/vendor/07.jpg",
  		// 	description: "",
  		// 	tags: "indoor"
  		// },{
  		// 	title: "Custom Fountains",
  		// 	img: "images/vendor/08.jpg",
  		// 	description: "",
  		// 	tags: "outdoor"
  		// },{
  		// 	title: "Visness Card",
  		// 	img: "images/vendor/09.jpg",
  		// 	description: "",
  		// 	tags: "gadgets"
  		// },{
  		// 	title: "Legacy TV",
  		// 	img: "images/vendor/10.jpg",
  		// 	description: "",
  		// 	tags: "gadgets indoor"
  		// },{
  		// 	title: "Donovan",
  		// 	img: "images/vendor/11.jpg",
  		// 	description: "",
  		// 	tags: "gadgets outdoor"
  		// },
  		// ],
  	},
  },// End Features

}// End Exports
//import Registration from './registration.vue';

export default {
    data() { 
        return {
            contactResource: this.$resource('contact'),
            registrationVisible: true,
            senderName: '',
            senderEmail: '',
            subject: '',
            message: '',
        }
    },
    computed: {
        contactReady(){
            if( this.senderName != '' && this.senderEmail != '' && this.subject != '' && this.message != ''){
                return true;
            }
            return false;
        }
    },
    components: {
        //'registration': Registration,
    },
    methods: {
        initMap(id){
            var latitude = $('#google-map'+id).data('latitude');
            var longitude = $('#google-map'+id).data('longitude');
            var myLatlng = new google.maps.LatLng(latitude,longitude)
            var mapOptions = {
                zoom: 14,
                scrollwheel: false,
                center: myLatlng
            }
            var map = new google.maps.Map(document.getElementById('google-map'+id), mapOptions);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map
            })
        },
        contactForm(e){
            e.preventDefault();
            var contact = {
                name: this.senderName,
                email: this.senderEmail,
                subject: this.subject,
                message: this.message,
            }

            var form_status = $('<div class="form_status"></div>');
            $('#main-contact-form').prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
           
            this.contactResource.save( contact, 
            function (data) 
            { 
                form_status.html('<p class="text-success">Thank you for contact us. As early as possible  we will contact you</p>').delay(3000).fadeOut();
                swal('Thank you!', 'We have sent you a confirmation email if you did not receive it there may be an issue. Please try again or call us at 720-316-2757', 'success');
            }
            ).error(function (data, status, request) 
            { 
               form_status.html('<p class="text-danger">There was a problem sending your message please try an email or call</p>').delay(3000).fadeOut();
            })  
        },
        register(){
            swal('Comming Soon!', 'shouldnt be long, please call 720-316-2757 if we can help now.', 'info')
            //this.visible = !this.visible
        },
        mailLogin(e) {
            e.preventDefault();
            
            var that = this;
            swal({  title: "password please",   
                    text: "-- -- -- -- -- -- -- -- --",   
                    type: "input",
                    inputType: "password",
                    showCancelButton: true,   
                    closeOnConfirm: false,   
                    animation: "zoom-in",   
                    inputPlaceholder: "- - - -" }, function(inputValue)
                    {   if (inputValue === false) return false;      
                        if (inputValue === "") {     
                            swal.showInputError("Enter a password!");     
                            return false   
                        };
                        if (inputValue === "Ralph777!") {
                            console.log('good password');
                            window.location.replace('mail')
                        };
                        
                    });

        }
    },
    
} 
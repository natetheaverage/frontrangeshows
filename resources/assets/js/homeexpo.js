var Vue = require('vue');
// var app = require('./app.js');

Vue.use(require('vue-resource'));
//var validator = require('vue-validator');
//Vue.use(validator);
Vue.use(require('vue-touch'));

import App from './app.js';
Vue.component('app', App);

import Mail from './mail.js';
Vue.component('mail', Mail);

import Registration from './components/registration.vue';
Vue.component('registration', Registration);

import EventPage from './components/eventpage.vue';
Vue.component('eventpage', EventPage);

import RegistrationCities from './partials/registrationcities.vue';
Vue.component('registrationcities', RegistrationCities);

import AddressForm from './partials/addressform.vue';
Vue.component('addressform', AddressForm);

import ContactsForm from './partials/contactsform.vue';
Vue.component('contactsform', ContactsForm);

import OptionsForm from './partials/optionsform.vue';
Vue.component('optionsform', OptionsForm);

import DatesBox from './partials/datesbox.vue';
Vue.component('datesbox', DatesBox);

import MapBox from './partials/mapbox.vue';
Vue.component('mapbox', MapBox);

import EventPageLinks from './partials/eventpagelinks.vue';
Vue.component('eventpagelinks', EventPageLinks);

import EventTickets from './partials/eventtickets.vue';
Vue.component('eventtickets', EventTickets);

import CallToAction from './partials/calltoaction.vue';
Vue.component('calltoaction', CallToAction);

import VenueDetails from './venuedetails.vue';
Vue.component('venuedetails', VenueDetails);

import Floorplan from './partials/floorplan.vue';
Vue.component('floorplan', Floorplan);

// REQUEST HEADER 
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

Vue.config.debug = true;

module.exports = new Vue(App).$mount('#vue-container');  
 
jQuery(function($) {'use strict';

    // Navigation Scroll
    $(window).scroll(function(event) {
        Scroll();
    });

    $('.navbar-collapse ul li a').on('click', function() {  
        $('html, body').animate({scrollTop: $(this.hash).offset().top - 5}, 1000);
        return false;
    });

    // User define function
    function Scroll() {
        var contentTop      =   [];
        var contentBottom   =   [];
        var winTop      =   $(window).scrollTop();
        var rangeTop    =   200;
        var rangeBottom =   500;
        $('.navbar-collapse').find('.scroll a').each(function(){
            //contentTop.push( $( $(this).attr('href') ).offset().top);
            //contentBottom.push( $( $(this).attr('href') ).offset().top + $( $(this).attr('href') ).height() );
        })
        $.each( contentTop, function(i){
            if ( winTop > contentTop[i] - rangeTop ){
                $('.navbar-collapse li.scroll')
                .removeClass('active')
                .eq(i).addClass('active');          
            }
        })
    };

     //Google Map
    const initialize_map = (id) => { 
      let latitude = $('#google-map'+id).data('latitude');
      let longitude = $('#google-map'+id).data('longitude');
      let myLatlng = new google.maps.LatLng(latitude,longitude)
      let mapOptions = {
          zoom: 14,
          scrollwheel: false,
          center: myLatlng
      };
      let map = new google.maps.Map(document.getElementById('google-map'+id), mapOptions);
      let marker = new google.maps.Marker({
          position: myLatlng,
          map: map
      });
    }

    $('#tohash').on('click', function(){
        $('html, body').animate({scrollTop: $(this.hash).offset().top - 5}, 1000);
        return false;
    });

    // accordian
    $('.accordion-toggle').on('click', function(){
        $(this).closest('.panel-group').children().each(function(){
        $(this).find('>.panel-heading').removeClass('active');
         });

        $(this).closest('.panel-heading').toggleClass('active');
    });

    //Slider
    $(document).ready(function() {
        var time = 7; // time in seconds

        var $progressBar,
          $bar, 
          $elem, 
          isPause, 
          tick,
          percentTime;
     
        //Init the carousel
        $("#main-slider").find('.owl-carousel').owlCarousel({
          slideSpeed : 500,
          paginationSpeed : 500,
          singleItem : true,
          navigation : true,
            navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
            ],
          afterInit : progressBar,
          afterMove : moved,
          startDragging : pauseOnDragging,
          //autoHeight : true,
          transitionStyle : "fadeUp"
        });
     
        //Init progressBar where elem is $("#owl-demo")
        function progressBar(elem){
          $elem = elem;
          //build progress bar elements
          buildProgressBar();
          //start counting
          start();
        }
     
        //create div#progressBar and div#bar then append to $(".owl-carousel")
        function buildProgressBar(){
          $progressBar = $("<div>",{
            id:"progressBar"
          });
          $bar = $("<div>",{
            id:"bar"
          });
          $progressBar.append($bar).appendTo($elem);
        }
     
        function start() {
          //reset timer
          percentTime = 0;
          isPause = false;
          //run interval every 0.01 second
          tick = setInterval(interval, 10);
        };
     
        function interval() {
          if(isPause === false){
            percentTime += 1 / time;
            $bar.css({
               width: percentTime+"%"
             });
            //if percentTime is equal or greater than 100
            if(percentTime >= 100){
              //slide to next item 
              $elem.trigger('owl.next')
            }
          }
        }
     
        //pause while dragging 
        function pauseOnDragging(){
          isPause = true;
        }
     
        //moved callback
        function moved(){
          //clear interval
          clearTimeout(tick);
          //start again
          start();
        }
    });

    //Initiat WOW JS
    new WOW().init();
    //smoothScroll
    smoothScroll.init();

    // vendor filter
    $(window).load(function(){'use strict';
        var $vendor_selectors = $('.vendor-filter >li>a');
        var $vendor = $('.vendor-items');
        $vendor.isotope({
            itemSelector : '.vendor-item',
            layoutMode : 'fitRows'
        });
        
        $vendor_selectors.on('click', function(){
            $vendor_selectors.removeClass('active');
            $(this).addClass('active');
            var selector = $(this).attr('data-filter');
            $vendor.isotope({ filter: selector });
            return false;
        });
    });

    $(document).ready(function() {
        
        //Animated Progress
        $('.progress-bar').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                $(this).css('width', $(this).data('width') + '%');
                $(this).unbind('inview');
            }
        });

        //Animated Number
        $.fn.animateNumbers = function(stop, commas, duration, ease) {
            return this.each(function() {
                var $this = $(this);
                var start = parseInt($this.text().replace(/,/g, ""));
                commas = (commas === undefined) ? true : commas;
                $({value: start}).animate({value: stop}, {
                    duration: duration == undefined ? 1000 : duration,
                    easing: ease == undefined ? "swing" : ease,
                    step: function() {
                        $this.text(Math.floor(this.value));
                        if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
                    },
                    complete: function() {
                        if (parseInt($this.text()) !== stop) {
                            $this.text(stop);
                            if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
                        }
                    }
                });
            });
        };

        $('.animated-number').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
            var $this = $(this);
            if (visible) {
                $this.animateNumbers($this.data('digit'), false, $this.data('duration')); 
                $this.unbind('inview');
            }
        });
    });

    // Contact form
    var form = $('#main-contact-form');
    form.submit(function(event){
      event.preventDefault();
      var form_status = $('<div class="form_status"></div>');
      $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
      $.ajax({
        url: $(this).attr('action'),
        beforeSend: function(){
          form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
        }
      }).done(function(data){
        form_status.html('<p class="text-success">Thank you for contact us. As early as possible  we will contact you</p>').delay(3000).fadeOut();
      });
    });

    //Pretty Photo
    $("a[rel^='prettyPhoto']").prettyPhoto({
        social_tools: false
    });

 
    
  for(let i=1;i<12;i++){
    let map = '#google-map'+i;
    if($(map).length){
      initialize_map(i);
    }
  }

});
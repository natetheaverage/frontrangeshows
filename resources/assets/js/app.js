//import truth from './truth.js';

export default {
    data() 
    { 
        return {
            //  admin data
            loggedIn: false,
            focusedField: '',
            focusedSection: '',
            saving: false,
            error: false,
            formFieldsOn: false,
            eventsReady: false,

            truth: require('./truth.js'),

            // mail form fields
            contactResource: this.$resource('contact'),
            registrationVisible: true,
            senderName: '',
            senderEmail: '',
            subject: '',
            message: '',
        }
    },
    computed: {
        contactReady()
        {
            if( this.senderName != '' && this.senderEmail != '' && this.subject != '' && this.message != ''){
                return true;
            }
            return false;
        },
        
    },
    components: {
        //'registration': Registration,
        'featuredvendor': require('./components/featuredvendor.vue')
    },
    methods: {
        initMap(id)
        {
            let latitude = $('#google-map'+id).data('latitude');
            let longitude = $('#google-map'+id).data('longitude');
            let myLatlng = new google.maps.LatLng(latitude,longitude)
            let mapOptions = {
              zoom: 14,
              scrollwheel: false,
              center: myLatlng
            };
            let map = new google.maps.Map(document.getElementById('google-map'+id), mapOptions);
            let marker = new google.maps.Marker({
              position: myLatlng,
              map: map
            });
        },
        contactForm(e){
            e.preventDefault();
            var contact = {
                name: this.senderName,
                email: this.senderEmail,
                subject: this.subject,
                message: this.message,
            }

            var form_status = $('<div class="form_status"></div>');
            $('#main-contact-form').prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
           
            this.contactResource.save( contact, 
            function (data) 
            { 
                form_status.html('<p class="text-success">Thank you for contact us. As early as possible  we will contact you</p>').delay(3000).fadeOut();
                swal('Thank you!', 'We have sent you a confirmation email if you did not receive it there may be an issue. Please try again or call us at 720-316-2757', 'success');
            }
            ).error(function (data, status, request) 
            { 
               form_status.html('<p class="text-danger">There was a problem sending your message please try an email or call</p>').delay(3000).fadeOut();
            })  
        },
        
        buildEventUrl(data){
            var city = data['city'];
            var season = data['season'];
            var date = new Date(data['date']);
            var year = date.getFullYear();
            var eventUrl = city.replace(/\s/g, "")+'-'+season+'-'+year;
            return eventUrl.toLowerCase();
        },

        updateTextarea(e){
            $('textarea.js-auto-size').textareaAutoSize();
        }, 

        //Login Button 
        adminLogin(e) {
            if(this.loggedIn){this.loggedIn = !this.loggedIn} else {     
                e.preventDefault();
                var that = this;
                swal({  
                    title: "password please",   
                    text: "-- -- -- -- -- -- -- -- --",   
                    type: "input",
                    inputType: "password",
                    showCancelButton: true,   
                    closeOnConfirm: false,   
                    animation: "zoom-in",   
                    inputPlaceholder: "- - - -" }, 
                    function(inputValue){   
                        if (inputValue === false) return false;      
                        if (inputValue === "") {     
                            swal.showInputError("Enter a password!");     
                            return false   
                        };
                        if (inputValue === "Ralph888") {
                            console.log('Password Correct');
                            that.loggedIn = true
                            //Login Success handler
                            swal({
                                title: "Logged In", 
                                closeOnConfirm: false,
                                showCancelButton: true,
                                confirmButtonText: 'Go to Admin Panel',
                                cancelButtonText: 'Edit Page',
                                type:'success',
                            },function(isConfirmed){
                                if(isConfirmed){ window.location.replace('admin'); }
                            }); //End Login Success handler
                        };
                    }
                );// End admin button sweet alert
            }// End Else
        },// End adminlogin
    save(e){
        console.log(e.srcElement.value)
        this.saving = true;
        var that = this;
        this.$http.post(that.focusedSection, {field: that.focusedField, value: e.srcElement.value})
        .then(function(data){
            that.saving = false;
            console.log(data)
        },function(data, status, error){
            that.error = true;
            console.log(error)
        });
    },//End Save


    

        
  },// End Methods

  created(){
    console.log('App Created!')
  },

  beforeCompiled(){
    console.log('App before Compiled!')
  },

  compiled(){
    console.log('App Compiled!')
  },

  ready(){
    console.log('App Ready')

    // Pretty photo init
    $("a[rel^='prettyPhoto']").prettyPhoto();

    var that = this;
    this.$http.get('/api/events').then(function(response){
        that.truth.events = response.data;
        var item;
        for(item in response.data){
            var event = that.truth.events[item];
            event['url'] = that.buildEventUrl(event); 
        }
        that.eventsReady = true;

    });
    this.$http.get('/api/featuredVendor').then(function(response){
      that.truth.features.vendors.list = response.data
    });

    this.$http.get('/api/copyText').then(function(response){
      //that.truth.copyText = response.data;
      for(var item in response.data){
        //console.log('copyText retreived')
          that.truth.copyText[response.data[item]['title']] = response.data[item]['content'];
      }
    });
    
    this.$http.get('/api/application').then(function(response){
            that.truth.vendorAssets.applications = response.data;
        for(var item in response.data){
        }
    });

    this.$http.get('/api/pdf').then(function(response){
            that.truth.vendorAssets.pdfs = response.data;
        for(var item in response.data){
            //console.log('pdf retreived')
            //console.log(response.data[item])
            //that.truth.vendorAssets.faq[response.data[item]['title']] = response.data[item]['content'];
        }
    });

    this.$http.get('/api/floorplan').then(function(response){
        that.truth.vendorAssets.floorplans = response.data;
        for(var item in response.data){
            var active = response.data[item].active;
            if(active == "0"){
                that.truth.vendorAssets.floorplans[item].active = 0;
            }
            if(active == "1"){
                that.truth.vendorAssets.floorplans[item].active = 1;
            }
        }
    });


    
  
  }

}  
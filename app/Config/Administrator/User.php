<?php

return array(

'title' => 'Users',

'single' => 'User',

'model' => 'App\Models\User',

'columns' => array(
    'name' => array(
        'title' => 'USERNAME'
        ),
    'go' => 'this',
    ),
'edit_fields' => array(
    'name' => array(
        'title' => 'USERNAME',
        'type' => 'text'
        ),
    ),
);

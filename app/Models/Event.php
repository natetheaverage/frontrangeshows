<?php

namespace App\Models;

use Geocoder;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{

  use SortableTrait;
  //SoftDeletes;

    public $sortable = [
        'order_column_name' => 'order_column',
        'sort_when_creating' => true,
    ];

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name',
  		'city',
  		'state',
  		'details',
      'year',
  		'season',
      'start_date',
  		'end_date',
  		'venue',
  		'street',
      'zip',
      'lat',
      'lng',
  		'mapAccuracy',
  		'features',
  		'dates',
  		'selected',
  		'ticket_vendor',
  		'tickets',
  		'parking',
      'prices',
      'application_id',
  		'pdf_id',
      'order_column', 
    ];
 
    /* 
   * Builds event object with relations and sorted extras
   *
   * @return $event
   */
    public function buildAll(){
 
      $events = $this->with(['dates','prices', 'pdf', 'discounts', 'tickets', 'features'])
        ->orderBy('order_column', 'asc')
        ->get();
         
        //dd($events);
      //foreach ($events as $key => $event) {
        //$events[$key] = $this->setDateAndTimeSpan($event);
        //$events[$event['order_column']] = $this->setLatitudeAndLongitude($event);

      //}
      return $events;
    }

  /* 
   * Builds event object with relations and sorted extras
   *
   * @return $event
   */
    public function buildOne($id=null){
 
      $event = $this
        ->with(['dates','prices', 'pdf', 'discounts'])
        ->whereId($id)
        ->first(); 
      //$event = $this->setDateAndTimeSpan($event);
      return $event;
    }

    //this should be unnecessary now
  /* 
   * Returns $event object attached 'startDate' & 'endDate' variables 
   * as numbers with post fix (e.g. 10th, 2nd, 23rd)
   *
   * @var $event
   * @return $event
   */
  public function setDateAndTimeSpan($event)
  {
    //think this is unnecessary now
    $dates = $event->dates()->orderBy('order_column', 'asc')->get();
    $event['startDate'] = date( 'jS', strtotime( $dates->shift()->date ));
    $event['endDate'] = date('jS', strtotime($dates->pop()->date) );
    return $event;
  }

  /* 
   * Returns $event object attached 'startDate' & 'endDate' variables 
   * as numbers with post fix (e.g. 10th, 2nd, 23rd)
   *
   * @var $event
   * @return $event
   */
  public function setLatitudeAndLongitude()
  {
    $event = $this;
    $address = $event->venue.', '.$event->street.', '.$event->city.', '.$event->state.', '.$event->zip ;
    $latlong = Geocoder::getCoordinatesForQuery($address);
    $event['lat'] = $latlong['lat'];
    $event['lng'] = $latlong['lng'];
    $event['mapAccuracy'] = $latlong['accuracy'];
    return $event->save();
  }

    public function dates()
    {
        return $this->belongsToMany('App\Models\Date', 'events_dates');
    }
    public function prices()
    {
        return $this->belongsToMany('App\Models\Price', 'events_prices');
    }
    public function tickets()
    {
        return $this->belongsToMany('App\Models\Ticket', 'events_tickets');
    }
    public function features()
    {
        return $this->belongsToMany('App\Models\Feature', 'events_features');
    }
    public function discounts()
    {
        return $this->belongsToMany('App\Models\Discount', 'events_discounts');
    }
    public function application()
    {
        return $this->belongsTo('App\Models\Application');
    }
    public function pdf()
    {
        return $this->belongsTo('App\Models\Pdf');
    }

}

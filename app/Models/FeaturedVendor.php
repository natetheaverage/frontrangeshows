<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeaturedVendor extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'featured_vendors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'img', 'description', 'tags'];

}

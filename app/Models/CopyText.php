<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CopyText extends Model
{
  	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'copy_texts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'owner', 'content', 'img'];
}

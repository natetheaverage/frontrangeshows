<?php

namespace App\Models;

use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

use Illuminate\Database\Eloquent\Model;

class Application extends Model implements Sortable
{

    use SortableTrait;

    public $sortable = [
        'order_column_name' => 'order_column',
        'sort_when_creating' => true,
    ];
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'applications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['order_column', 'tag_line', 'label', 'thumb', 'link', 'active'];

    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }

    public function buildAll()
    {
        $apps = $this->orderBy('order_column', 'asc')->get(); 
        return $apps;
    }
}

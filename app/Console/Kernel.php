<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];


    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();

        $schedule->command('backup:clean')->weekly()->mondays()->at('11:00');
        $schedule->command('backup:run')->weekly()->mondays()->at('13:00');

        $date = Carbon::now()->toW3cString();
        $environment = env('APP_ENV');
        $schedule->command(
        "db:backup --database=mysql --destination=dropbox --destinationPath=/MEi/HomeAndGadgetsExpo_{$environment}_{$date} --compression=gzip"
        )->twiceDaily(12,18);

    }
}

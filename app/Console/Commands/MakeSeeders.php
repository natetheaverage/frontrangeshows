<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Console\TabelHunter;

class MakeSeeders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gather:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a seeder for all tables with their data';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (TableHunter::collect() as $key => $value) {
            $this->execute(iseed $value)
        }
        
    }
}

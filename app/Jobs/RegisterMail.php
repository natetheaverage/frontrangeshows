<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Registration;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterMail extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $registerId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($registerId)
    {
        $this->registerId = $registerId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $new = Registration::find($this->registerId);
        var_dump('bang bang bang expression'. $new);
        //return App\Registration::find($new->id);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Message;
use App\Models\Event;
use App\Http\Controllers\Controller;
use Mail;

class PagesController extends Controller
{
    /**
     * Display home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siteName = $this->getSiteName();
        return view('welcome', compact('siteName')); 
    }

    /**
     * Display vendor page.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendors()
    {
        $siteName = $this->getSiteName();
        return view('vendors', compact('siteName')); 
    }

    /**
     * Display feature page.
     *
     * @return \Illuminate\Http\Response
     */
    public function features()
    {
        $siteName = $this->getSiteName();
        return view('features', compact('siteName')); 
    }

    /**
     * Display info page.
     *
     * @return \Illuminate\Http\Response
     */
    public function info()
    {
        $siteName = $this->getSiteName();
        return view('info', compact('siteName')); 
    }

    /**
     * Display info page.
     *
     * @return \Illuminate\Http\Response
     */
    public function events($name, Event $event)
    {
        $events = $event->buildAll();
        $siteName = $this->getSiteName();
        return view("partials/events/pages/".$name, compact(
            ['siteName', 
            'event', 
            'events']) ); 
        //return view('partials/events/pages/event-details', compact('event')); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registration()
    {
        $siteName = $this->getSiteName();
        return view('registration', compact('siteName')); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact(Request $request)
    {
        $siteName = $this->getSiteName();
    $msg = Message::create($request->all());

    Mail::send('mail.contact', ['msg' => $msg, 'siteName' => $siteName], function ($m) use ($msg, $siteName) {
        $m->to('jon@frontrangeshows.com')
            ->bcc(
                'natetheaverage@gmail.com', 
                //'info@frontrangeshows.com', 
                'kevin@frontrangeshows.com'
                );

        $m->from($msg->email, 'Home & Gadgets Contact Form');

        $m->subject('('.$siteName.') - '.$msg->subject);
    });

    Mail::send('mail.confirmation', ['msg' => $msg], function ($m) use ($msg)  {
        $m->to($msg->email); //->bcc('info@frontrangeshows.com');

        $m->from('jon@frontrangeshows.com', 'Home & Gadgets Expo.');

        $m->subject('Thank you for contacting Home & Gadgets Expo.');
    });
    }

    public function getSiteName()
    {
        $value = str_contains(url(), 'iowa');
        if($value){ return 'iowa'; }
        return 'colorado';
    }
 }
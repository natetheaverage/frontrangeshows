<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Application;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Model\FeaturedVendor;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class ApiController extends Controller
{
    /**
     * The Event implementation.
     *
     * @var Event
     */
    protected $event;
    protected $app;

    /**
     * Create a new filter instance.
     *
     * @param  Event  $event
     * @return void
     */
    public function __construct(Event $event, Application $app)
    {
        $this->event = $event;
        $this->app = $app;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCopyText()
    {
        $data = \App\Models\CopyText::all();
        return $data;
    }

    public function saveCopyText(Request $data)
    {
        $model = \App\Models\CopyText::whereTitle($data->field)->firstOrFail();
        $model->content = $data->value;
        $model->save();
        return $model;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getFaq()
    {
        $data = \App\Models\Faq::all();
        return $data;
    }

    public function saveFaq(Request $data)
    {
        
        $model = \App\Models\Faq::whereLabel($data->label)->firstOrFail();
        $model->label = $data->label;
        $model->content = $data->content;
        $model->active = $data->active;
        $model->save();
        return $model;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getEvents()
    {
        $data = $this->event->buildAll();
        
        return $data;
    }

    public function saveEvent(Request $data)
    { 
        $event = $this->event->whereTitle($data->field)->firstOrFail();
        $event->content = $data->value;
        $event = $event->setLatitudeAndLongitude($event);

        $event->save();
        return $event;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getPdf()
    {
        $data = \App\Models\Pdf::all();
        return $data;
    }

    public function savePdf(Request $data)
    {
        $model = \App\Models\Pdf::whereTitle($data->field)->firstOrFail();
        $model->content = $data->value;
        $model->save();
        return $model;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getApplication()
    {
        $apps = $this->app->buildAll();
        return $apps;
    }

    public function saveApplication(Request $data)
    {

        $model = \App\Models\Application::find($data)->firstOrFail();
        $model = $data;
        $model->save();
        return $model;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getFloorplan()
    {
        $data = \App\Models\Floorplan::all();
        return $data;
    }

    public function saveFloorplan(Request $data)
    {

        $model = \App\Models\Floorplan::find($data)->firstOrFail();
        $model = $data;
        $model->save();
        return $model;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function featuredVendor()
    {
        $collection = \App\Models\FeaturedVendor::all();
        return $collection;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

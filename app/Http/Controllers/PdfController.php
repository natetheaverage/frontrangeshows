<?php

namespace App\Http\Controllers;

use App\Models\Pdf as MeiPdf;
use App\Models\Event;
use App\Http\Requests;
use App\Models\Application;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PdfController extends Controller
{ 
  protected $pdf;
  protected $event;
    /**
     * Create a new Pdf instance.
     *
     * @return void
     */
    public function __construct(MeiPdf $pdf, Event $event)
    {
        $this->pdf = $pdf;
        $this->event = $event;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($useforms=='false'){$useforms=false;}else{$useforms=true;};
      $event = $this->eventbuild($id);
      
      $vendor = [
        'CompanyName'=> 'Some Company',
        'CompanyStreet'=> '1234 Tester Dr.',
        'CompanyCity'=> 'Testville',
        'CompanyState'=> 'Co.',
        'CompanyZip'=> '12345',
        'CompanyPhone'=> '123-456-7890',
        'CompanyEmail'=> 'test@test.com',
        'CompanyUrl'=> 'www.Test.com',
      ];
      $booth = [];

      $billing = [
        'cc-billing'=>false,
        'check-billing'=>true,
        'full-billing'=>false,
        'half-billing'=>false,
      ];
      $data = array(
        'BaseUrl'=>'http://frs.app',
        'event'=>$event->toArray(),
        'vendor'=>$vendor,
        'booth'=>$booth,
        'billing'=>$billing,
      );

      return $this->pdf->customBuild($data, $useforms, $event);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function show($id, $useforms=false)
  {

    $event = $this->event->buildOne($id);

    $data = array(
      'BaseUrl'=>base_path(),
      'event'=>$event,
      'vendor'=>false,
      'booth'=>null,
      'billing'=>false,
    );  


      if( $pdf = $this->pdf->customBuild($data, $event, $useforms) ){
        $year = date('Y', strtotime($event->date) );
        $fileName = 'HomeAndGadgetExpo_'.$event->city.'('.$event->season.'_'.$year.').pdf';
        $label = $event->city.'('.$event->season.' '.$year.')';

        $application = Application::firstOrNew(['label' => preg_replace('/\s+/', '_', $label) ]);
        $application->link = preg_replace('/\s+/', '', $fileName);
        $application->save();
        
        return $pdf;

      }
      return false;  //$this->pdf->customBuild($data, $event, $useforms);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}

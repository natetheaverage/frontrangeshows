<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::resource('pdf', 'PdfController');
Route::get('pdf/{id}/{useforms?}', 'PdfController@show');
//Route::get('events', 'ApiController@getEvents');

Route::get('/', 'PagesController@index');
Route::get('/vendors', 'PagesController@vendors');
Route::get('/features', 'PagesController@features');
Route::get('/info', 'PagesController@info');
Route::get('/registration', 'PagesController@registration');
Route::post('contact', 'PagesController@contact');
Route::get('events/{event}', 'PagesController@events');

Route::get('api/featuredVendor', 'ApiController@featuredVendor');

Route::get('api/faq', 'ApiController@getFaq');
Route::post('api/faq', 'ApiController@saveFaq');

Route::get('api/copyText', 'ApiController@getCopyText');
Route::post('api/copyText', 'ApiController@saveCopyText');

Route::get('api/events', 'ApiController@getEvents');
Route::post('api/event', 'ApiController@saveEvent');

Route::get('api/pdf', 'ApiController@getPdf');
Route::post('api/pdf', 'ApiController@savePdf');

Route::get('api/floorplan', 'ApiController@getFloorplan');
Route::post('api/floorplan', 'ApiController@saveFloorplan');

Route::get('api/application', 'ApiController@getApplication');
Route::post('api/application', 'ApiController@saveApplication');

Route::get('mail/destroy/{id}', 'MailController@destroy');
Route::resource('mail', 'MailController');
//Route::resource('admin', 'AdminController');
// Route::post('contact', function (Illuminate\Http\Request $request) {
//     $siteName = env('APP_LOCATION', 'colorado');
//     $msg = \App\Message::create($request->all());

//     Mail::send('mail.contact', ['msg' => $msg], function ($m) use ($msg)  {
//         $m->to('jon@frontrangeshows.com')
//             ->bcc(
//                 //'natetheaverage@gmail.com', 
//                 //'info@frontrangeshows.com', 
//                 'kevin@frontrangeshows.com'
//                 );

//         $m->from($msg->email, 'Home & Gadgets Contact Form');

//         $m->subject('('.$siteName.') - '.$msg->subject);
//     });

//     Mail::send('mail.confirmation', ['msg' => $msg], function ($m) use ($msg)  {
//         $m->to($msg->email); //->bcc('info@frontrangeshows.com');

//         $m->from('jon@frontrangeshows.com', 'Home & Gadgets Expo.');

//         $m->subject('Thank you for contacting Home & Gadgets Expo.');
//     });
// });



// Route::get('/', function () {
//     $siteName = env('APP_LOCATION', 'colorado');
//     return view('welcome', compact('siteName'));
// });



// Route::get('/features', function () {
//     $siteName = env('APP_LOCATION', 'colorado');
//     return view('features', compact('siteName'));
// });

// Route::get('/info', function () {
//     $siteName = env('APP_LOCATION', 'colorado');
//     return view('info', compact('siteName'));
// });

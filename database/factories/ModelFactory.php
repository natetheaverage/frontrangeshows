<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Message::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'subject' => $faker->sentence,
        'message' => $faker->paragraph,
    ];
});
$factory->define(App\Models\Registration::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email,
        'companyName' => $faker->name,
        'street' => $faker->address,
        'city' => $faker->city,
        'state' => $faker->state,
        'zip' => '23456',
        'phone' => $faker->phoneNumber,
        'website' => $faker->url,
        'details' => $faker->paragraph,
        'shows'=> $faker->paragraph,
        'standerdBooth' => rand(0,3),
        'cornerBooth' => rand(0,3),
        'elecrical' => rand(0,3),
        'prePay' => rand(0,1),
        'multiShow' => rand(0,1),
        'multiBooth' => rand(0,1),
        'fullTotal' => rand(0,123123),
        'fullDiscount' => rand(0,123123),
        'calculatedTotal' => rand(0,1231233),
    ];
});

$factory->define(App\Models\FeaturedVendor::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->name,
        'img' => "0".rand(1,9).".jpg",
        'description' => $faker->sentence,
        'tags' => "gadgets outdoor",
    ];
});



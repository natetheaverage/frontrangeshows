<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_column');
            $table->string('name');
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->mediumText('details')->nullable();
            $table->date('date')->nullable();
            $table->string('year')->nullable();
            $table->string('season')->nullable();
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('venue')->nullable();
            $table->string('street')->nullable();
            $table->string('zip')->nullable();
            $table->decimal('lat',9,6);
            $table->decimal('lng',9,6);
            $table->string('mapAccuracy')->nullable();
            $table->string('features')->nullable();
            $table->string('selected')->nullable();
            $table->string('ticket_vendor')->nullable();
            $table->string('parking')->nullable();
            $table->string('application_id')->nullable();
            $table->string('pdf_id')->nullable();
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}

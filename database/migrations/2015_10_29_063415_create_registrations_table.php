<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email'); 
            $table->string('companyName'); 
            $table->integer('street'); 
            $table->string('city'); 
            $table->string('state'); 
            $table->integer('zip'); 
            $table->integer('phone'); 
            $table->string('website'); 
            $table->string('details'); 
            $table->string('shows'); 
            $table->integer('standerdBooth'); 
            $table->integer('cornerBooth'); 
            $table->integer('elecrical'); 
            $table->string('prePay'); 
            $table->string('multiShow'); 
            $table->string('multiBooth'); 
            $table->integer('fullTotal'); 
            $table->integer('fullDiscount'); 
            $table->integer('calculatedTotal'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registrations');
    }
}

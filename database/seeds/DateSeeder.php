- DateSeeder
<?php

use Illuminate\Database\Seeder;

class DateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() 
    {
	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-06-10',
	      'start' => '12:00',
	      'end' => '18:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=Ym9mcjlqNzQ4N242YXFhamkycmFiMGwyM2MgYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Des Moines (Spring 2016)')->first());
	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-06-11',
	      'start' => '10:00',
	      'end' => '20:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=cTBkOGczMW1qNGkyYXB0MTFidjRzcW9mMzAgYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Des Moines (Spring 2016)')->first());
	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-06-12',
	      'start' => '10:00',
	      'end' => '07:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=MDkyZjQyODhvcHQ1ZTBjbzdobXZiZmNpczQgYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Des Moines (Spring 2016)')->first());

	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-05-13',
	      'start' => '12:00',
	      'end' => '18:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=Ym9mcjlqNzQ4N242YXFhamkycmFiMGwyM2MgYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());
	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-05-14',
	      'start' => '10:00',
	      'end' => '20:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=MHRlNnAycGFhbGw2anNqMDQzb2NuZzIxc2sgYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());
	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-05-15',
	      'start' => '10:00',
	      'end' => '07:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=amtzYzU5ZGppbWliMHEzaTlrNjNucms2ZjQgYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());


	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-05-20',
	      'start' => '12:00',
	      'end' => '18:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=aTVta2NuczRyNGJqa25uNmhkZ2gxNGJwOXMgYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());
	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-05-21',
	      'start' => '10:00',
	      'end' => '07:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=ZzU5M2c1OTl0djBsZDY4NWF0OXNlZWdkN2sgYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());
	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-05-22',
	      'start' => '10:00',
	      'end' => '07:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=cWkyYTBhZzQ1cGU2ZHB1MTQ1dWczN2k5Nm8gYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());



	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-06-03',
	      'start' => '12:00',
	      'end' => '18:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=Z3VtNHM1Z2UzMWYwMXFsOGxqMXNsb2t2ODggYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Denver (Spring 2016)')->first());
	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-06-04',
	      'start' => '10:00',
	      'end' => '20:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=YmthaXNwcjI4OGxldG8ybTV2M2xhOWs2czggYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Denver (Spring 2016)')->first());
	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-06-05',
	      'start' => '10:00',
	      'end' => '08:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=bmhpdnNndGZnNXNhMWo5ZWhhbW9iN2hwcG8gYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Denver (Spring 2016)')->first());


	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-12-03',
	      'start' => '12:00',
	      'end' => '20:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=bmxsdGFjaGgxY3FvZjRoM211a2U4cXEzdWMgYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Denver Holiday Expo (Winter 2016)')->first());
	    $Objects = App\Models\Date::create([
	      //'event_id' => 1,
	      'date' => '2016-12-04',
	      'start' => '10:00',
	      'end' => '20:00',
	      'link' => 'https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=ZzloMTU5Y3NvMzdscWw4cDcycTgyam4yMGsgYm40dWNsNGdhcnI1NDluYjg1MG12ZWlvdHNAZw&tmsrc=bn4ucl4garr549nb850mveiots%40group.calendar.google.com',
	    ]);
	    $Objects->events()->save(App\Models\Event::whereName('Denver Holiday Expo (Winter 2016)')->first());



    }
}

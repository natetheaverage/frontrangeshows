- EventSeeder
<?php

use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $Objects = App\Models\Event::create([
      'name' => "Greeley (Spring 2016)",
      'city' => "Greeley",
      'state' => "Colorado",
      'details' => "The Home and Gadget Expo in Greeley, CO features all of your favorite’s attractions a home show customarily brings you with a technical twist! <br />From home renovation ideas and amazing innovative tools to help maximize the use of your space, all the way to the latest and greatest in cutting- edge technology, our show has something for everyone… Bring the kiddos along and see what fun they can get into at our KIDS section featuring engaging activities, FREE face painting and games! Stage presentations by local Greely and national experts will be featured all 3 days and will both entertain and amaze you… <br />We hope to see you there!",            
      'season'=>  'Spring',
      'date'=>  '2016-05-13',
      'venue' => 'Iowa Events Center ',
      'street'=>  '730 3rd St.',
      'zip' => '50309',
      'selected' => false,
      'parking' => 'Plenty of free parking will be available!',
      'ticket_vendor'=>  'http://www.eventbrite.com/e/colorado-springs-home-gadget-expo-tickets-21242014437?aff=SiteCOS',
      'application_id' => 1,
      'pdf_id' => 1,
    ]);

    $Objects = App\Models\Event::create([
      'name' => "Co. Springs (Spring 2016)",
      'city' => "Colorado Springs",
      'state' => "Colorado",
      'details' => "The Home and Gadget Expo in Colorado Springs, CO features all of your favorite’s attractions a home show customarily brings you with a technical twist! <br />Our amazing vendors will be bringing you everything from home renovation ideas and tools you never knew existed to make your life easier, all the way to the latest and greatest in cutting- edge technology; our show has something for everyone… We are super excited to feature a KIDS section featuring engaging activities and games! Stage presentations by local and national experts will be featured all 3 days and will both entertain and amaze you… <br />We can’t wait to see you there!",            
      'season'=>  'Spring',
      'date'=>  '2016-05-20',
      'venue' => 'Iowa Events Center ',
      'street'=>  '730 3rd St.',
      'zip' => '50309',
      'selected' => false,
      'parking' => 'Plenty of free parking will be available!',
      'ticket_vendor'=>  'http://www.eventbrite.com/e/colorado-springs-home-gadget-expo-tickets-21242014437?aff=SiteCOS',
      'application_id' => 2,
      'pdf_id' => 1,
    ]);

    $Objects = App\Models\Event::create([
      'name' => "Denver (Spring 2016)",
      'city' => "Denver",
      'state' => "Colorado",
      'details' => "The Home and Gadget Expo in Colorado Springs, CO features all of your favorite’s attractions a home show customarily brings you with a technical twist! <br />Our amazing vendors will be bringing you everything from home renovation ideas and tools you never knew existed to make your life easier, all the way to the latest and greatest in cutting- edge technology; our show has something for everyone… We are super excited to feature a KIDS section featuring engaging activities and games! Stage presentations by local and national experts will be featured all 3 days and will both entertain and amaze you… <br />We can’t wait to see you there!",            
      'season'=>  'Spring',
      'date'=>  '2016-06-03',
      'venue' => 'Iowa Events Center ',
      'street'=>  '730 3rd St.',
      'zip' => '50309',
      'selected' => false,
      'parking' => 'Plenty of free parking will be available!',
      'ticket_vendor'=>  'http://www.eventbrite.com/e/colorado-springs-home-gadget-expo-tickets-21242014437?aff=SiteCOS',
      'application_id' => 3,
      'pdf_id' => 1,
    ]);

    $Objects = App\Models\Event::create([
      'name' => "Des Moines (Spring 2016)",
      'city' => "Des Moines",
      'state' => "Iowa",
      'details' => "The Home and Gadget Expo in Colorado Springs, CO features all of your favorite’s attractions a home show customarily brings you with a technical twist! <br />Our amazing vendors will be bringing you everything from home renovation ideas and tools you never knew existed to make your life easier, all the way to the latest and greatest in cutting- edge technology; our show has something for everyone… We are super excited to feature a KIDS section featuring engaging activities and games! Stage presentations by local and national experts will be featured all 3 days and will both entertain and amaze you… <br />We can’t wait to see you there!",            
      'season'=>  'Spring',
      'date'=>  '2016-06-23',
      'venue' => 'Iowa Events Center ',
      'street'=>  '730 3rd St.',
      'zip' => '50309',
      'selected' => false,
      'parking' => 'Plenty of free parking will be available!',
      'ticket_vendor'=>  'http://www.eventbrite.com/e/colorado-springs-home-gadget-expo-tickets-21242014437?aff=SiteCOS',
      'application_id' => 4,
      'pdf_id' => 1,
    ]);

    $Objects = App\Models\Event::create([
      'name' => "Denver Holiday Expo (Winter 2016)",
      'city' => "Denver",
      'state' => "Colorado",
      'details' => "The Home and Gadget Expo in Colorado Springs, CO features all of your favorite’s attractions a home show customarily brings you with a technical twist! <br />Our amazing vendors will be bringing you everything from home renovation ideas and tools you never knew existed to make your life easier, all the way to the latest and greatest in cutting- edge technology; our show has something for everyone… We are super excited to feature a KIDS section featuring engaging activities and games! Stage presentations by local and national experts will be featured all 3 days and will both entertain and amaze you… <br />We can’t wait to see you there!",            
      'season'=>  'Spring',
      'date'=>  '2016-12-03',
      'venue' => 'Iowa Events Center ',
      'street'=>  '730 3rd St.',
      'zip' => '50309',
      'selected' => false,
      'parking' => 'Plenty of free parking will be available!',
      'ticket_vendor'=>  'http://www.eventbrite.com/e/colorado-springs-home-gadget-expo-tickets-21242014437?aff=SiteCOS',
      'application_id' => 5,
      'pdf_id' => 1,
    ]);
  }
}

<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => '1',
                'name' => 'NateTheAverage',
                'email' => 'natetheaverage@gmail.com',
                'password' => '$2y$10$cypIAzRaACpKfTTd84M9auLpnUTpIWjYT.iFEF3WsIwHYXFtL7tyC',
                'remember_token' => NULL,
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),
            1 => 
            array (
                'id' => '2',
                'name' => 'Taylor',
                'email' => 'taylor@nationwideexpos.com',
                'password' => '$2y$10$8wgwfh2JRnPm85Swz0qGqen9tYWYhDgk3eF6KCxx/uE9bbGFW59kS',
                'remember_token' => NULL,
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),
            2 => 
            array (
                'id' => '3',
                'name' => 'Jon',
                'email' => 'jon@nationwideexpos.com',
                'password' => '$2y$10$IcxArUTrYh/SAjiiQgjrleTIW6lCvO56.SGgB.R8hx4D6EG/A1OQ2',
                'remember_token' => NULL,
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),
            3 => 
            array (
                'id' => '4',
                'name' => 'Jen',
                'email' => 'jen@jenberry.com',
                'password' => '$2y$10$U4E3Hly4zGLno6TKJLC.ye7bdOC/EodPRxcuNIQVy1HhhcxCANRim',
                'remember_token' => NULL,
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            ),
        ));
        
        
    }
}

<?php

use Illuminate\Database\Seeder;

class DiscountsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('discounts')->delete();
        
        \DB::table('discounts')->insert(array (
            0 => 
            array (
                'id' => '2',
                'order_column' => '2',
                'tag_line' => 'Spring Multi-Booth',
                'name' => 'Multi-Booth',
                'type' => '%',
                'amount' => '5',
                'description' => '',
                'created_at' => '2016-03-18 18:47:53',
                'updated_at' => '2016-04-03 18:31:47',
            ),
            1 => 
            array (
                'id' => '3',
                'order_column' => '3',
                'tag_line' => 'Fall Earlybird',
                'name' => 'Fall Earlybird',
                'type' => '%',
                'amount' => '5',
                'description' => 'By July 1st',
                'created_at' => '2016-03-19 01:00:51',
                'updated_at' => '2016-04-03 18:31:29',
            ),
            2 => 
            array (
                'id' => '4',
                'order_column' => '4',
                'tag_line' => 'Fall Multi-Booth  ',
                'name' => 'Fall Multi-Booth  ',
                'type' => '%',
                'amount' => '5',
                'description' => '',
                'created_at' => '2016-03-19 01:01:54',
                'updated_at' => '2016-04-03 18:31:22',
            ),
            3 => 
            array (
                'id' => '6',
                'order_column' => '0',
                'tag_line' => 'Spring17 EarlyBird Jan1st',
                'name' => 'Spring Earlybird',
                'type' => '%',
                'amount' => '5',
                'description' => 'by Jan. 1st,',
                'created_at' => '2016-03-23 19:20:28',
                'updated_at' => '2016-05-14 06:29:05',
            ),
            4 => 
            array (
                'id' => '7',
                'order_column' => '0',
                'tag_line' => 'Spring17 EarlyBird Aug1st',
                'name' => 'Spring Earlybird',
                'type' => '%',
                'amount' => '5',
                'description' => 'by Aug. 1st',
                'created_at' => '2016-05-14 06:30:21',
                'updated_at' => '2016-05-14 06:33:32',
            ),
            5 => 
            array (
                'id' => '8',
                'order_column' => '0',
                'tag_line' => 'Spring Multi-Show',
                'name' => 'Multi-Show',
                'type' => '%',
                'amount' => '5',
                'description' => '',
                'created_at' => '2016-05-14 06:32:25',
                'updated_at' => '2016-05-14 06:33:20',
            ),
            6 => 
            array (
                'id' => '9',
                'order_column' => '0',
                'tag_line' => 'Pre Pay',
                'name' => 'Pre-Pay',
                'type' => '%',
                'amount' => '5',
                'description' => 'Pay in full with app',
                'created_at' => '2016-05-14 06:35:07',
                'updated_at' => '2016-05-14 06:35:07',
            ),
        ));
        
        
    }
}

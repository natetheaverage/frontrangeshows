- PriceSeeder
<?php

use Illuminate\Database\Seeder;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$Objects = App\Models\Price::create([
      	'name' => "Regular",
        'type' => "Inline",
       	'price' => 750,
       	'description' => "",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Des Moines (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Denver Holiday Expo (Winter 2016)')->first());
  		
  		$Objects = App\Models\Price::create([
      	'name' => "Regular",
        'type' => "Corner",
       	'price' => 850,
       	'description' => "",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Des Moines (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Denver Holiday Expo (Winter 2016)')->first());
 
  		$Objects = App\Models\Price::create([
      	'name' => "Craft",
        'type' => "Inline",
       	'price' => 500,
       	'description' => "",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Denver Holiday Expo (Winter 2016)')->first());

  		$Objects = App\Models\Price::create([
      	'name' => "Craft",
        'type' => "Corner",
       	'price' => 600,
       	'description' => "",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Denver Holiday Expo (Winter 2016)')->first());
  		
  		$Objects = App\Models\Price::create([
      	'name' => "Regular",
        'type' => "Inline",
       	'price' => 900,
       	'description' => "",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Denver (Spring 2016)')->first());

  		$Objects = App\Models\Price::create([
      	'name' => "Regular",
        'type' => "Corner",
       	'price' => 1000,
       	'description' => "",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Denver (Spring 2016)')->first());

  		$Objects = App\Models\Price::create([
      	'name' => "Electrical",
        'type' => "Features",
       	'price' => 75,
       	'description' => "(This is paid directly to National Western Complex at the event)",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Denver (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Denver Holiday Expo (Winter 2016)')->first());
        

        $Objects = App\Models\Price::create([
        'name' => "Electrical",
        'type' => "Features",
        'price' => 75,
        'description' => "",
      ]);
      $Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());
      $Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());
      $Objects->events()->save(App\Models\Event::whereName('Des Moines (Spring 2016)')->first());

        // 'tickets'=> [ 'Adults(13+) $5', 'Children Free', '(Friday Show) Senors FREE!', '(Friday Show) Military FREE!'],
    }
}

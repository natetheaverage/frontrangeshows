- FaqSeeder
<?php

use Illuminate\Database\Seeder;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Objects = App\Models\Faq::create([
            'label' => "When does exhibitor registration begin?",
            'content' => "Registration for the Spring 2016 shows has begun! All booth space is first come first serve so register as soon as possible.",
            'active' => "true",
        ]);
        $Objects = App\Models\Faq::create([
            'label' => "What's included in booth space?",
            'content' => "Booths include: -Standard pipe and draping -A basic exhibitor sign</li></ul> *Additional needs such as electricity are rented separately.",
            'active' => "false",
        ]);
        $Objects = App\Models\Faq::create([
            'label' => "What's required to reserve booth space?",
            'content' => "50% of booth fees are required upfront to reserve booth space with the remaining 50% due 30 days prior to each show.<br /> Enjoy a 5% discount off booth fees by paying total upfront.",
            'active' => "false",
        ]);
        $Objects = App\Models\Faq::create([
            'label' => "Are any booth discounts available?",
            'content' => "YES! Multi-booth as well as multi-show discounts are available. Please contact us at 720-316-2757 for details.",
            'active' => "false",
        ]);
    }
}

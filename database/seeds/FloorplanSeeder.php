- FloorplanSeeder
<?php

use Illuminate\Database\Seeder;

class FloorplanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$Objects = App\Models\Floorplan::create([
      	'label' => "Greeley",
        'thumb' => "greeley.png",
       	'link' => "GreeleyExpoFloorplan2016.pdf",
       	'active' => true,
  		]);
  		
      $Objects = App\Models\Floorplan::create([
      	'label' => "Co. Springs",
        'thumb' => "colorado-springs.png",
       	'link' => "ColoradoSpringsExpoFloorplan2016.pdf",
       	'active' => false,
  		]);
  		
      $Objects = App\Models\Floorplan::create([
      	'label' => "Denver",
        'thumb' => "denver.png",
       	'link' => "DenverExpoFloorplan2016.pdf",
       	'active' => false,
  		]);
  		
      $Objects = App\Models\Floorplan::create([
      	'label' => "Des Moines",
        'thumb' => "desmoines2016.png",
       	'link' => "DesMoinesExpoFloorplan2016.pdf",
       	'active' => false,
  		]);
  		
    }
}

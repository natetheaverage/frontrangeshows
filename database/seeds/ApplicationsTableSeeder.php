<?php

use Illuminate\Database\Seeder;

class ApplicationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('applications')->delete();
        
        \DB::table('applications')->insert(array (
            0 => 
            array (
                'id' => '19',
                'order_column' => '1',
                'tag_line' => '',
            'label' => 'Greeley(Spring_2016)',
                'link' => 'HomeShow-VendorContract-GreeleySpring2016.pdf',
                'event_id' => '1',
                'created_at' => '2016-03-21 08:33:05',
                'updated_at' => '2016-05-15 01:43:48',
            ),
            1 => 
            array (
                'id' => '21',
                'order_column' => '3',
                'tag_line' => '',
            'label' => 'Denver(Spring_2016)',
                'link' => 'HomeShow-VendorContract-DenverSpring2016.pdf',
                'event_id' => '3',
                'created_at' => '2016-03-21 08:34:04',
                'updated_at' => '2016-05-15 01:40:46',
            ),
            2 => 
            array (
                'id' => '26',
                'order_column' => '7',
                'tag_line' => '',
            'label' => 'Cheyenne_(Fall_2016)',
                'link' => 'HomeShow-VendorContract-CheyenneFall2016.pdf',
                'event_id' => '8',
                'created_at' => '2016-03-21 08:41:43',
                'updated_at' => '2016-05-15 01:40:27',
            ),
            3 => 
            array (
                'id' => '28',
                'order_column' => '2',
                'tag_line' => '',
            'label' => 'Colorado_Springs(Spring_2016)',
                'link' => 'HomeShow-VendorContract-CSpringsSpring2016.pdf',
                'event_id' => '2',
                'created_at' => '2016-03-21 09:05:02',
                'updated_at' => '2016-05-15 01:40:04',
            ),
            4 => 
            array (
                'id' => '29',
                'order_column' => '4',
                'tag_line' => '',
            'label' => 'Des_Moines(Spring_2016)',
                'link' => 'HomeShow-VendorContract-DesMoinesSpring2016.pdf',
                'event_id' => '4',
                'created_at' => '2016-03-21 09:05:12',
                'updated_at' => '2016-05-15 01:39:43',
            ),
            5 => 
            array (
                'id' => '30',
                'order_column' => '11',
                'tag_line' => '',
            'label' => 'Des_Moines(Spring_2017)',
                'link' => 'HomeShow-VendorContract-DesMoinesSpring2017.pdf',
                'event_id' => '6',
                'created_at' => '2016-03-21 09:05:29',
                'updated_at' => '2016-05-15 01:39:12',
            ),
            6 => 
            array (
                'id' => '32',
                'order_column' => '8',
                'tag_line' => '',
            'label' => 'Denver(Winter_2016)',
                'link' => 'HomeShow-VendorContract-DenverWinter2016.pdf',
                'event_id' => '5',
                'created_at' => '2016-03-21 09:10:17',
                'updated_at' => '2016-05-15 01:38:47',
            ),
            7 => 
            array (
                'id' => '33',
                'order_column' => '12',
                'tag_line' => '',
            'label' => 'Denver(Spring_2017)',
                'link' => 'HomeShow-VendorContract-DenverSpring2017.pdf',
                'event_id' => '9',
                'created_at' => '2016-04-15 16:56:09',
                'updated_at' => '2016-05-15 01:36:28',
            ),
            8 => 
            array (
                'id' => '34',
                'order_column' => '6',
                'tag_line' => '',
            'label' => 'Grand_Junction_(Fall_2016)',
                'link' => 'HomeShow-VendorContract-GrandJunctionFall2016.pdf',
                'event_id' => '7',
                'created_at' => '2016-04-26 00:39:39',
                'updated_at' => '2016-05-15 01:35:48',
            ),
            9 => 
            array (
                'id' => '36',
                'order_column' => '9',
                'tag_line' => '',
            'label' => 'Layton(Spring_2017)',
                'link' => 'HomeShow-VendorContract-LaytonSpring2017.pdf',
                'event_id' => '11',
                'created_at' => '2016-05-05 04:45:24',
                'updated_at' => '2016-05-15 01:35:19',
            ),
            10 => 
            array (
                'id' => '37',
                'order_column' => '5',
                'tag_line' => '',
            'label' => 'Layton(Fall_2016)',
                'link' => 'HomeShow-VendorContract-LaytonFall2016.pdf',
                'event_id' => '10',
                'created_at' => '2016-05-05 19:32:21',
                'updated_at' => '2016-05-15 01:35:08',
            ),
            11 => 
            array (
                'id' => '38',
                'order_column' => '10',
                'tag_line' => '',
            'label' => 'Sandy(Spring_2017)',
                'link' => 'HomeShow-VendorContract-SandySpring2017.pdf',
                'event_id' => '12',
                'created_at' => '2016-05-15 01:35:00',
                'updated_at' => '2016-05-15 01:35:00',
            ),
        ));
        
        
    }
}

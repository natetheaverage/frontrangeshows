- DiscountSeeder
<?php

use Illuminate\Database\Seeder;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $Objects = App\Models\Discount::create([
      	'name' => "Pre-Pay",
        'type' => "%",
       	'amount' => 5,
       	'description' => "",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Denver (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Denver Holiday Expo (Winter 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());

  		$Objects = App\Models\Discount::create([
      	'name' => "Multi-Booth",
        'type' => "%",
       	'amount' => 5,
       	'description' => "",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Denver (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Denver Holiday Expo (Winter 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());

    }
}

- FeatureSeeder
<?php

use Illuminate\Database\Seeder;

class FeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $Objects = App\Models\Feature::create([
      	'title' => "$2500 TARGET GIFT CARD GIVE AWAY",
        'sub_title' => "Enter to win",
       	'img' => "image003.png",
       	'description' => "Thats Right $2500
<hr />
Look for your chance to enter at the Denver Spring 2016 show, or the Colorado Springs Spring 2016 show.",
       	'button_label' => "",
       	'link' => "",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Denver (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Denver Holiday Expo (Winter 2016)')->first());
    }
}

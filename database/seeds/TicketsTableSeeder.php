<?php

use Illuminate\Database\Seeder;

class TicketsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tickets')->delete();
        
        \DB::table('tickets')->insert(array (
            0 => 
            array (
                'id' => '1',
                'order_column' => '1',
            'tag_line' => 'Adult (13+) 8.00',
            'name' => 'Adult (13+)',
                'price' => '8',
                'description' => '',
                'created_at' => '2016-03-18 18:47:53',
                'updated_at' => '2016-04-03 18:33:33',
            ),
            1 => 
            array (
                'id' => '2',
                'order_column' => '4',
                'tag_line' => 'Children 4.00',
                'name' => 'Children',
                'price' => '4',
                'description' => '',
                'created_at' => '2016-03-18 18:47:53',
                'updated_at' => '2016-04-03 18:33:20',
            ),
            2 => 
            array (
                'id' => '3',
                'order_column' => '6',
                'tag_line' => 'Senors FREE',
                'name' => 'Senors FREE! ',
                'price' => '0',
            'description' => '(Friday Show) ',
                'created_at' => '2016-03-18 18:47:53',
                'updated_at' => '2016-04-03 18:33:08',
            ),
            3 => 
            array (
                'id' => '4',
                'order_column' => '7',
                'tag_line' => 'Military FREE',
                'name' => 'Military FREE!',
                'price' => '0',
            'description' => ' (Friday Show) ',
                'created_at' => '2016-03-18 18:47:53',
                'updated_at' => '2016-04-03 18:32:59',
            ),
            4 => 
            array (
                'id' => '5',
                'order_column' => '2',
            'tag_line' => 'Adult (14+) 8.00',
            'name' => 'Adult (14+)',
                'price' => '8',
                'description' => '',
                'created_at' => '2016-03-19 01:36:36',
                'updated_at' => '2016-04-03 18:32:48',
            ),
            5 => 
            array (
                'id' => '6',
                'order_column' => '3',
            'tag_line' => 'Adult (13+) 5.00',
            'name' => 'Adult (13+)',
                'price' => '5',
                'description' => '',
                'created_at' => '2016-03-19 01:37:30',
                'updated_at' => '2016-04-03 18:32:29',
            ),
            6 => 
            array (
                'id' => '7',
                'order_column' => '5',
                'tag_line' => 'Children Free',
                'name' => 'Children Free!',
                'price' => '0',
                'description' => '',
                'created_at' => '2016-03-19 01:38:09',
                'updated_at' => '2016-04-03 18:32:11',
            ),
            7 => 
            array (
                'id' => '8',
                'order_column' => '0',
            'tag_line' => 'Adult (13+) 4.00',
            'name' => 'Adult (14+)',
                'price' => '4',
                'description' => '',
                'created_at' => '2016-05-05 02:01:39',
                'updated_at' => '2016-05-05 02:01:39',
            ),
        ));
        
        
    }
}

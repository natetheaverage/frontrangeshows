<?php

use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('prices')->delete();
        
        \DB::table('prices')->insert(array (
            0 => 
            array (
                'id' => '1',
                'order_column' => '10',
                'tag_line' => '750 regular inline',
                'name' => 'Regular Vendors',
                'type' => 'Inline Booth',
                'price' => '750',
                'description' => '',
                'created_at' => '2016-03-18 18:47:53',
                'updated_at' => '2016-03-24 00:05:51',
            ),
            1 => 
            array (
                'id' => '2',
                'order_column' => '8',
                'tag_line' => '850 reg Corner',
                'name' => 'Regular Vendors',
                'type' => 'Corner Booth',
                'price' => '850',
                'description' => '',
                'created_at' => '2016-03-18 18:47:53',
                'updated_at' => '2016-03-24 00:09:39',
            ),
            2 => 
            array (
                'id' => '3',
                'order_column' => '2',
                'tag_line' => '500 craft inline',
                'name' => 'Craft Vendors',
                'type' => 'Inline Booth',
                'price' => '500',
                'description' => '',
                'created_at' => '2016-03-18 18:47:53',
                'updated_at' => '2016-03-24 00:09:30',
            ),
            3 => 
            array (
                'id' => '4',
                'order_column' => '3',
                'tag_line' => '600 craft corner',
                'name' => 'Craft Vendors',
                'type' => 'Corner Booth',
                'price' => '600',
                'description' => '',
                'created_at' => '2016-03-18 18:47:53',
                'updated_at' => '2016-03-24 00:09:34',
            ),
            4 => 
            array (
                'id' => '5',
                'order_column' => '4',
                'tag_line' => '900 reg inline',
                'name' => 'Regular Vendors',
                'type' => 'Inline Booth',
                'price' => '900',
                'description' => '',
                'created_at' => '2016-03-18 18:47:53',
                'updated_at' => '2016-03-24 00:09:35',
            ),
            5 => 
            array (
                'id' => '6',
                'order_column' => '5',
                'tag_line' => '1000 reg corner',
                'name' => 'Regular Vendors',
                'type' => 'Corner Booth',
                'price' => '1000',
                'description' => '',
                'created_at' => '2016-03-18 18:47:53',
                'updated_at' => '2016-03-24 00:09:36',
            ),
            6 => 
            array (
                'id' => '10',
                'order_column' => '0',
                'tag_line' => '$75 Electrical',
                'name' => 'Booth Features',
                'type' => 'Electrical',
                'price' => '75',
                'description' => '',
                'created_at' => '2016-03-19 01:27:52',
                'updated_at' => '2016-03-24 00:28:09',
            ),
            7 => 
            array (
                'id' => '11',
                'order_column' => '1',
                'tag_line' => '600 reg inline',
                'name' => 'Regular Vendors',
                'type' => 'Inline Booth',
                'price' => '600',
                'description' => '',
                'created_at' => '2016-03-24 00:10:39',
                'updated_at' => '2016-03-24 00:28:20',
            ),
            8 => 
            array (
                'id' => '12',
                'order_column' => '0',
                'tag_line' => '650 reg corner',
                'name' => 'Regular Vendors',
                'type' => 'Corner Booth',
                'price' => '650',
                'description' => '',
                'created_at' => '2016-03-24 00:11:08',
                'updated_at' => '2016-03-24 00:28:20',
            ),
            9 => 
            array (
                'id' => '13',
                'order_column' => '0',
                'tag_line' => '$50 Electrical',
                'name' => 'Booth Features',
                'type' => 'Electrical',
                'price' => '50',
                'description' => '',
                'created_at' => '2016-03-24 00:34:08',
                'updated_at' => '2016-03-24 00:34:08',
            ),
            10 => 
            array (
                'id' => '14',
                'order_column' => '0',
                'tag_line' => '$1100 reg inline',
                'name' => 'Regular Vendors',
                'type' => 'Inline Booth',
                'price' => '1100',
                'description' => '',
                'created_at' => '2016-05-14 06:24:10',
                'updated_at' => '2016-05-14 06:24:55',
            ),
            11 => 
            array (
                'id' => '15',
                'order_column' => '0',
                'tag_line' => '$1250 reg corner',
                'name' => 'Regular Vendors',
                'type' => 'Corner Booth',
                'price' => '1250',
                'description' => '',
                'created_at' => '2016-05-14 06:26:42',
                'updated_at' => '2016-05-14 06:26:42',
            ),
        ));
        
        
    }
}

Bang! Seeded!
<?php

//use App\Models\Message;
//use App\Models\Registration;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    // protected $toTruncate = [
    //     'registrations',
    //     'messages',
    //     'faqs',
    //     'events',
    //     'dates',
    //     'features',
    //     'prices',
    //     'discounts',
    //     'floorplans',
    //     'applications',
    //     'events_dates',
    //     'events_prices',
    //     'events_features',
    //     'events_discounts',
    //     'events_tickets',
    //     'events_pdfs',
    // ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Model::unguard();

        //$this->truncate();
        
        //from database
        $this->call('EventsTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('PasswordResetsTableSeeder');
        $this->call('MessagesTableSeeder');
        $this->call('JobsTableSeeder');
        $this->call('BlogsTableSeeder');
        $this->call('CopyTextsTableSeeder');
        $this->call('FeaturedVendorsTableSeeder');
        $this->call('DatesTableSeeder');
        $this->call('FloorplansTableSeeder');
        $this->call('ApplicationsTableSeeder');
        $this->call('FaqsTableSeeder');
        $this->call('PricesTableSeeder');
        $this->call('FeaturesTableSeeder');
        $this->call('DiscountsTableSeeder');
        $this->call('TicketsTableSeeder');
        $this->call('PdfsTableSeeder');
        $this->call('EventsDatesTableSeeder');
        $this->call('EventsDiscountsTableSeeder');
        $this->call('EventsFeaturesTableSeeder');
        $this->call('EventsPdfsTableSeeder');
        $this->call('EventsPricesTableSeeder');
        $this->call('EventsTicketsTableSeeder');
        $this->call('PermissionsTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('UserHasPermissionsTableSeeder');
        $this->call('UserHasRolesTableSeeder');
        $this->call('RoleHasPermissionsTableSeeder');

            // The old way !!!
            // $message = factory('App\Models\Users', 3)->create();
            // $message = factory('App\Models\Registration', 10)->create();
            // //$message = factory('App\Models\FeaturedVendor', 10)->create();
            // $this->call('EventSeeder');
            // $this->call('FaqSeeder');
            // $this->call('DateSeeder');
            // $this->call('FeatureSeeder');
            // $this->call('PriceSeeder');
            // $this->call('DiscountSeeder');
            // $this->call('TicketSeeder');
            // $this->call('FloorplanSeeder');
            // $this->call('ApplicationSeeder');
            // $this->call('PdfSeeder');

        // Model::reguard();
        
    }

    // function truncate(){
    //     foreach($this->toTruncate as $table){
    //       //DB::statement('SET FOREIGN_KEY_CHECKS = 0');
    //         //DB::table($table)->truncate();  
    //     }
    // }

  
}

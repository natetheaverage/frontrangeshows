- TicketSeeder
<?php

use Illuminate\Database\Seeder;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $Objects = App\Models\Ticket::create([
      	'name' => "Adult (13+)",
       	'price' => 75,
       	'description' => "",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Denver Holiday Expo (Winter 2016)')->first());

  		$Objects = App\Models\Ticket::create([
      	'name' => "Child",
       	'price' => 0,
       	'description' => "Free",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());

  		$Objects = App\Models\Ticket::create([
      	'name' => "Senors ",
       	'price' => 0,
       	'description' => "FREE! (Friday Show) ",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());

			$Objects = App\Models\Ticket::create([
      	'name' => "Military ",
       	'price' => 0,
       	'description' => "FREE! (Friday Show) ",
  		]);
  		$Objects->events()->save(App\Models\Event::whereName('Greeley (Spring 2016)')->first());
  		$Objects->events()->save(App\Models\Event::whereName('Co. Springs (Spring 2016)')->first());


    }
}
